#!/usr/bin/env python

import sys
import json
import unicodedata
import re


USER_ID = sys.argv[1]

def slugify(value):
    """
    Converts to lowercase, removes non-word characters (alphanumerics and
    underscores) and converts spaces to hyphens. Also strips leading and
    trailing whitespace.
    """
    value = unicodedata.normalize('NFKD', value).encode('ascii', 'ignore').decode('ascii')
    value = re.sub('[^\w\s-]', '', value).strip().lower()
    return re.sub('[-\s]+', '-', value)

sf = open('./source.json')
tf = open('./src/target.js', 'w')
data = json.load(sf)
features = data['features']
target_features = []
for feature in features:
    if True or feature['validated']: # nothing is validated
        slug = slugify(feature['properties']['titre'])
        feature['properties']['slug'] = slug
        if 'photo' in feature['properties']:
            # photo_str = feature['properties']['photo'][5:]
            # spec, photo_b64 = photo_str.split(',')
            # mime = spec.split(';')[0]
            # fmt = mime.split('/')[1]
            # fname = '{}.{}'.format(slug, fmt)
            fname = '{}/{}'.format(USER_ID, slug)
            print('replace photo data with reference to: {}'.format(fname))
            feature['properties']['photo'] = fname
        target_features.append(feature)

exports = 'module.exports = exports = ' + json.dumps(target_features, indent=2) + ';'
tf.write(exports)
