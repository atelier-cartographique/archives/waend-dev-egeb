



URL=$1
COOKIE=$2

python <<EOF
import json
from datetime import datetime
f = open('bundle.js')
program = f.read()
target = open('data.json', 'w')
ts = datetime.isoformat(datetime.now())
data = {
    "id": "7027518e-b2c6-4d88-8710-93ae52813cbc",
    "user_id": "b08f26d7-63d8-436c-8325-a770d79ed2ba",
    "properties": {
        "lastmod": ts,
        "name": "layout",
        "description": "layout program",
        "program": program,
        "style":{
            "strokeStyle":"#ce2929"
        }
    }
}
json.dump(data, target, indent=2)
EOF



curl ${URL}\
     -X PUT \
     -H 'Pragma: no-cache' \
     -H 'Origin: https://alpha.waend.com' \
     -H 'Accept-Encoding: gzip, deflate, sdch, br' \
     -H 'Accept-Language: en,en-US;q=0.8' \
     -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/53.0.2785.143 Chrome/53.0.2785.143 Safari/537.36' \
     -H 'Content-Type: application/json; charset="UTF-8"' \
     -H 'Accept: */*' \
     -H 'Cache-Control: no-cache' \
     -H 'Cookie: connect.sid='${COOKIE} \
     -H 'Connection: keep-alive' \
     -H 'DNT: 1' \
     --data-binary  @data.json --compressed > /dev/null


WID=$(xdotool search  --name 'wænd' | head -1)
xdotool windowactivate $WID
xdotool key 'ctrl+r'
