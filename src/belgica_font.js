module.exports = {
    "fontInfo": {
        "ascent": 800,
        "descent": 200,
        "fontName": "Belgika Stroke",
        "unitsPerEm": 1000
    },
    "glyphs": {
        "32": {"glyph": [], "width": 600},
        "100": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 110.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 310.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 412.0,
                            "y": 0.0
                        },
                        "control2": {
                            "x": 514.0,
                            "y": 39.0
                        },
                        "end": {
                            "x": 592.0,
                            "y": 117.0
                        },
                        "start": {
                            "x": 310.0,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 748.0,
                            "y": 273.0
                        },
                        "control2": {
                            "x": 748.0,
                            "y": 527.0
                        },
                        "end": {
                            "x": 592.0,
                            "y": 683.0
                        },
                        "start": {
                            "x": 592.0,
                            "y": 117.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 514.0,
                            "y": 761.0
                        },
                        "control2": {
                            "x": 410.0,
                            "y": 800.0
                        },
                        "end": {
                            "x": 309.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 592.0,
                            "y": 683.0
                        }
                    },
                    "type": "C"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 20.0,
            "width": 779
        },
        "101": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 470.0,
                            "y": 400.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 400.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 470.0,
                            "y": 400.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 400.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 110.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 550.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 110.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 550.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 10.0,
            "width": 610
        },
        "102": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 470.0,
                            "y": 400.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 400.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 110.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 550.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 110.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": -40.0,
            "width": 560
        },
        "103": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 753.0,
                            "y": 425.0
                        },
                        "start": {
                            "x": 532.0,
                            "y": 425.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 753.0,
                            "y": 683.0
                        },
                        "start": {
                            "x": 753.0,
                            "y": 425.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 675.0,
                            "y": 761.0
                        },
                        "control2": {
                            "x": 572.0,
                            "y": 800.0
                        },
                        "end": {
                            "x": 470.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 753.0,
                            "y": 683.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 368.0,
                            "y": 800.0
                        },
                        "control2": {
                            "x": 265.0,
                            "y": 761.0
                        },
                        "end": {
                            "x": 187.0,
                            "y": 683.0
                        },
                        "start": {
                            "x": 470.0,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 109.0,
                            "y": 605.0
                        },
                        "control2": {
                            "x": 70.0,
                            "y": 502.0
                        },
                        "end": {
                            "x": 70.0,
                            "y": 400.0
                        },
                        "start": {
                            "x": 187.0,
                            "y": 683.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 70.0,
                            "y": 298.0
                        },
                        "control2": {
                            "x": 109.0,
                            "y": 195.0
                        },
                        "end": {
                            "x": 187.0,
                            "y": 117.0
                        },
                        "start": {
                            "x": 70.0,
                            "y": 400.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 265.0,
                            "y": 39.0
                        },
                        "control2": {
                            "x": 367.0,
                            "y": 0.0
                        },
                        "end": {
                            "x": 469.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 187.0,
                            "y": 117.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 571.0,
                            "y": 0.0
                        },
                        "control2": {
                            "x": 674.0,
                            "y": 38.0
                        },
                        "end": {
                            "x": 752.0,
                            "y": 116.0
                        },
                        "start": {
                            "x": 469.0,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                }
            ],
            "leftBearing": 20.0,
            "rightBearing": 40.0,
            "width": 843
        },
        "104": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 588.0,
                            "y": 400.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 400.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 590.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 590.0,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 110.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 60.0,
            "width": 700
        },
        "105": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 122.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 122.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 72.0,
            "rightBearing": 72.0,
            "width": 244
        },
        "106": {
            "glyph": [
                {
                    "points": {
                        "control1": {
                            "x": 38.0,
                            "y": 722.0
                        },
                        "control2": {
                            "x": 50.0,
                            "y": 739.0
                        },
                        "end": {
                            "x": 65.0,
                            "y": 753.0
                        },
                        "start": {
                            "x": 30.0,
                            "y": 703.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 94.0,
                            "y": 782.0
                        },
                        "control2": {
                            "x": 135.0,
                            "y": 800.0
                        },
                        "end": {
                            "x": 179.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 65.0,
                            "y": 753.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 269.0,
                            "y": 800.0
                        },
                        "control2": {
                            "x": 341.0,
                            "y": 728.0
                        },
                        "end": {
                            "x": 341.0,
                            "y": 638.0
                        },
                        "start": {
                            "x": 179.0,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 341.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 341.0,
                            "y": 638.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": -20.0,
            "rightBearing": 40.0,
            "width": 431
        },
        "107": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 110.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 166.0,
                            "y": 400.0
                        },
                        "start": {
                            "x": 570.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 570.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 166.0,
                            "y": 400.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 0.0,
            "width": 620
        },
        "108": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 110.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 590.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": -20.0,
            "width": 620
        },
        "109": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 512.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 912.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 512.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 110.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 912.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 912.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 60.0,
            "width": 1022
        },
        "110": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 670.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 110.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 670.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 670.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 60.0,
            "width": 780
        },
        "111": {
            "glyph": [
                {
                    "points": {
                        "control1": {
                            "x": 249.0,
                            "y": 800.0
                        },
                        "control2": {
                            "x": 70.0,
                            "y": 621.0
                        },
                        "end": {
                            "x": 70.0,
                            "y": 400.0
                        },
                        "start": {
                            "x": 470.0,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 70.0,
                            "y": 179.0
                        },
                        "control2": {
                            "x": 249.0,
                            "y": 0.0
                        },
                        "end": {
                            "x": 470.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 70.0,
                            "y": 400.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 691.0,
                            "y": 0.0
                        },
                        "control2": {
                            "x": 870.0,
                            "y": 179.0
                        },
                        "end": {
                            "x": 870.0,
                            "y": 400.0
                        },
                        "start": {
                            "x": 470.0,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 870.0,
                            "y": 621.0
                        },
                        "control2": {
                            "x": 691.0,
                            "y": 800.0
                        },
                        "end": {
                            "x": 470.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 870.0,
                            "y": 400.0
                        }
                    },
                    "type": "C"
                }
            ],
            "leftBearing": 20.0,
            "rightBearing": 20.0,
            "width": 940
        },
        "112": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 348.0,
                            "y": 480.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 480.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 480.0,
                            "y": 480.0
                        },
                        "control2": {
                            "x": 587.0078125,
                            "y": 374.0
                        },
                        "end": {
                            "x": 588.0,
                            "y": 242.0
                        },
                        "start": {
                            "x": 348.0,
                            "y": 480.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 589.0,
                            "y": 109.0
                        },
                        "control2": {
                            "x": 483.0,
                            "y": 1.0
                        },
                        "end": {
                            "x": 350.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 588.0,
                            "y": 242.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 110.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 350.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 110.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": -0.007017530237817482,
            "width": 638
        },
        "113": {
            "glyph": [
                {
                    "points": {
                        "control1": {
                            "x": 693.0,
                            "y": 800.0
                        },
                        "control2": {
                            "x": 874.0,
                            "y": 621.0
                        },
                        "end": {
                            "x": 874.0,
                            "y": 400.0
                        },
                        "start": {
                            "x": 472.0,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 874.0,
                            "y": 179.0
                        },
                        "control2": {
                            "x": 693.0,
                            "y": 0.0
                        },
                        "end": {
                            "x": 472.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 874.0,
                            "y": 400.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 251.0,
                            "y": 0.0
                        },
                        "control2": {
                            "x": 70.0,
                            "y": 179.0
                        },
                        "end": {
                            "x": 70.0,
                            "y": 400.0
                        },
                        "start": {
                            "x": 472.0,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 70.0,
                            "y": 621.0
                        },
                        "control2": {
                            "x": 251.0,
                            "y": 800.0
                        },
                        "end": {
                            "x": 472.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 70.0,
                            "y": 400.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 872.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 638.0,
                            "y": 566.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 20.0,
            "rightBearing": 20.0,
            "width": 944
        },
        "114": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 288.9912109375,
                            "y": 400.0
                        },
                        "start": {
                            "x": 489.9912109375,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 109.9912109375,
                            "y": 0.0
                        },
                        "start": {
                            "x": 109.9912109375,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 311.9912109375,
                            "y": 0.0
                        },
                        "start": {
                            "x": 109.9912109375,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 422.9912109375,
                            "y": 0.0
                        },
                        "control2": {
                            "x": 511.0,
                            "y": 91.0
                        },
                        "end": {
                            "x": 509.9912109375,
                            "y": 202.0
                        },
                        "start": {
                            "x": 311.9912109375,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 508.9912109375,
                            "y": 312.0
                        },
                        "control2": {
                            "x": 419.9912109375,
                            "y": 400.0
                        },
                        "end": {
                            "x": 309.9912109375,
                            "y": 400.0
                        },
                        "start": {
                            "x": 509.9912109375,
                            "y": 202.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 309.9912109375,
                            "y": 400.0
                        },
                        "control2": {
                            "x": 187.9912109375,
                            "y": 400.0
                        },
                        "end": {
                            "x": 109.9912109375,
                            "y": 400.0
                        },
                        "start": {
                            "x": 309.9912109375,
                            "y": 400.0
                        }
                    },
                    "type": "C"
                }
            ],
            "leftBearing": 59.9912109375,
            "rightBearing": 20.000202958783348,
            "width": 580
        },
        "115": {
            "glyph": [
                {
                    "points": {
                        "control1": {
                            "x": 80.0,
                            "y": 703.0
                        },
                        "control2": {
                            "x": 94.0,
                            "y": 724.0
                        },
                        "end": {
                            "x": 112.0,
                            "y": 742.0
                        },
                        "start": {
                            "x": 70.0,
                            "y": 679.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 148.0,
                            "y": 778.0
                        },
                        "control2": {
                            "x": 198.0,
                            "y": 800.0
                        },
                        "end": {
                            "x": 253.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 112.0,
                            "y": 742.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 362.0,
                            "y": 800.0
                        },
                        "control2": {
                            "x": 450.0,
                            "y": 711.0
                        },
                        "end": {
                            "x": 450.0,
                            "y": 602.0
                        },
                        "start": {
                            "x": 253.0,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 450.0,
                            "y": 548.0
                        },
                        "control2": {
                            "x": 429.7685546875,
                            "y": 490.943359375
                        },
                        "end": {
                            "x": 379.0,
                            "y": 454.0
                        },
                        "start": {
                            "x": 450.0,
                            "y": 602.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 155.0,
                            "y": 291.0
                        },
                        "start": {
                            "x": 379.0,
                            "y": 454.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 118.0,
                            "y": 264.0
                        },
                        "control2": {
                            "x": 88.0,
                            "y": 207.0
                        },
                        "end": {
                            "x": 88.0,
                            "y": 162.0
                        },
                        "start": {
                            "x": 155.0,
                            "y": 291.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 88.0,
                            "y": 73.0
                        },
                        "control2": {
                            "x": 160.0,
                            "y": 0.0
                        },
                        "end": {
                            "x": 250.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 88.0,
                            "y": 162.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 294.0,
                            "y": 0.0
                        },
                        "control2": {
                            "x": 335.0,
                            "y": 18.0
                        },
                        "end": {
                            "x": 364.0,
                            "y": 47.0
                        },
                        "start": {
                            "x": 250.0,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 379.0,
                            "y": 61.0
                        },
                        "control2": {
                            "x": 391.0,
                            "y": 78.0
                        },
                        "end": {
                            "x": 399.0,
                            "y": 97.0
                        },
                        "start": {
                            "x": 364.0,
                            "y": 47.0
                        }
                    },
                    "type": "C"
                }
            ],
            "leftBearing": 20.0,
            "rightBearing": 20.0,
            "width": 520
        },
        "116": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 310.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 310.0,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 630.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": -10.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": -60.0,
            "rightBearing": -60.0,
            "width": 620
        },
        "117": {
            "glyph": [
                {
                    "points": {
                        "control1": {
                            "x": 730.0,
                            "y": 185.0
                        },
                        "control2": {
                            "x": 730.0,
                            "y": 480.0
                        },
                        "end": {
                            "x": 730.0,
                            "y": 480.0
                        },
                        "start": {
                            "x": 730.0,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 730.0,
                            "y": 657.0
                        },
                        "control2": {
                            "x": 587.0,
                            "y": 800.0
                        },
                        "end": {
                            "x": 410.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 730.0,
                            "y": 480.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 233.0,
                            "y": 800.0
                        },
                        "control2": {
                            "x": 90.0,
                            "y": 656.8974609375
                        },
                        "end": {
                            "x": 90.0,
                            "y": 480.0
                        },
                        "start": {
                            "x": 410.0,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 90.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 90.0,
                            "y": 480.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 40.0,
            "rightBearing": 40.0,
            "width": 820
        },
        "118": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 330.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 30.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 630.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 330.0,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": -20.0,
            "rightBearing": -20.0,
            "width": 660
        },
        "119": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 517.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 273.0,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 273.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 30.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 1004.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 760.0,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 760.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 517.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": -20.0,
            "rightBearing": -20.0,
            "width": 1034
        },
        "120": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 618.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 90.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 618.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 90.0,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 40.0,
            "rightBearing": 40.0,
            "width": 708
        },
        "121": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 319.0,
                            "y": 400.0
                        },
                        "start": {
                            "x": 319.0,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 319.0,
                            "y": 400.0
                        },
                        "start": {
                            "x": 30.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 609.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 319.0,
                            "y": 400.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": -20.0,
            "rightBearing": -20.0,
            "width": 639
        },
        "122": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 90.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 570.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 570.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 90.0,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 570.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 90.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 40.0,
            "rightBearing": 40.0,
            "width": 660
        },
        "123": {
            "glyph": [
                {
                    "points": {
                        "control1": {
                            "x": 158.2392578125,
                            "y": 400.0
                        },
                        "control2": {
                            "x": 197.099609375,
                            "y": 361.1396484375
                        },
                        "end": {
                            "x": 197.099609375,
                            "y": 312.900390625
                        },
                        "start": {
                            "x": 110.0,
                            "y": 400.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 197.099609375,
                            "y": 87.099609375
                        },
                        "start": {
                            "x": 197.099609375,
                            "y": 312.900390625
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 197.099609375,
                            "y": 38.8603515625
                        },
                        "control2": {
                            "x": 235.9599609375,
                            "y": 0.0
                        },
                        "end": {
                            "x": 284.19921875,
                            "y": 0.0
                        },
                        "start": {
                            "x": 197.099609375,
                            "y": 87.099609375
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 235.9599609375,
                            "y": 800.0
                        },
                        "control2": {
                            "x": 197.099609375,
                            "y": 761.1396484375
                        },
                        "end": {
                            "x": 197.099609375,
                            "y": 712.900390625
                        },
                        "start": {
                            "x": 284.19921875,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 197.099609375,
                            "y": 487.099609375
                        },
                        "start": {
                            "x": 197.099609375,
                            "y": 712.900390625
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 197.099609375,
                            "y": 438.8603515625
                        },
                        "control2": {
                            "x": 158.2392578125,
                            "y": 400.0
                        },
                        "end": {
                            "x": 110.0,
                            "y": 400.0
                        },
                        "start": {
                            "x": 197.099609375,
                            "y": 487.099609375
                        }
                    },
                    "type": "C"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 59.80078125,
            "width": 394
        },
        "124": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 110.0,
                            "y": 1000.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 60.0,
            "width": 220
        },
        "125": {
            "glyph": [
                {
                    "points": {
                        "control1": {
                            "x": 236.0,
                            "y": 400.0
                        },
                        "control2": {
                            "x": 197.0,
                            "y": 361.0
                        },
                        "end": {
                            "x": 197.0,
                            "y": 313.0
                        },
                        "start": {
                            "x": 284.0,
                            "y": 400.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 197.0,
                            "y": 87.0
                        },
                        "start": {
                            "x": 197.0,
                            "y": 313.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 197.0,
                            "y": 39.0
                        },
                        "control2": {
                            "x": 158.0,
                            "y": 0.0
                        },
                        "end": {
                            "x": 110.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 197.0,
                            "y": 87.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 158.0,
                            "y": 800.0
                        },
                        "control2": {
                            "x": 197.0,
                            "y": 761.0
                        },
                        "end": {
                            "x": 197.0,
                            "y": 713.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 197.0,
                            "y": 487.0
                        },
                        "start": {
                            "x": 197.0,
                            "y": 713.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 197.0,
                            "y": 439.0
                        },
                        "control2": {
                            "x": 236.0,
                            "y": 400.0
                        },
                        "end": {
                            "x": 284.0,
                            "y": 400.0
                        },
                        "start": {
                            "x": 197.0,
                            "y": 487.0
                        }
                    },
                    "type": "C"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 60.0,
            "width": 394
        },
        "126": {
            "glyph": [
                {
                    "points": {
                        "control1": {
                            "x": 339.103515625,
                            "y": 452.9501953125
                        },
                        "control2": {
                            "x": 316.830078125,
                            "y": 461.435546875
                        },
                        "end": {
                            "x": 294.5556640625,
                            "y": 461.435546875
                        },
                        "start": {
                            "x": 356.07421875,
                            "y": 435.9794921875
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 272.28125,
                            "y": 461.435546875
                        },
                        "control2": {
                            "x": 250.0078125,
                            "y": 452.9501953125
                        },
                        "end": {
                            "x": 233.037109375,
                            "y": 435.9794921875
                        },
                        "start": {
                            "x": 294.5556640625,
                            "y": 461.435546875
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 216.06640625,
                            "y": 419.0087890625
                        },
                        "control2": {
                            "x": 193.79296875,
                            "y": 410.5234375
                        },
                        "end": {
                            "x": 171.5185546875,
                            "y": 410.5234375
                        },
                        "start": {
                            "x": 233.037109375,
                            "y": 435.9794921875
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 149.244140625,
                            "y": 410.5234375
                        },
                        "control2": {
                            "x": 126.970703125,
                            "y": 419.0087890625
                        },
                        "end": {
                            "x": 110.0,
                            "y": 435.9794921875
                        },
                        "start": {
                            "x": 171.5185546875,
                            "y": 410.5234375
                        }
                    },
                    "type": "C"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 59.92578125,
            "width": 466
        },
        "161": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 110.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 188.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 60.0,
            "width": 220
        },
        "162": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 411.0,
                            "y": 863.0
                        },
                        "start": {
                            "x": 411.0,
                            "y": 799.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 411.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 411.0,
                            "y": -64.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 579.0,
                            "y": 764.0
                        },
                        "control2": {
                            "x": 500.0,
                            "y": 800.0
                        },
                        "end": {
                            "x": 411.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 637.0,
                            "y": 706.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 234.0,
                            "y": 800.0
                        },
                        "control2": {
                            "x": 90.0,
                            "y": 657.0
                        },
                        "end": {
                            "x": 90.0,
                            "y": 480.0
                        },
                        "start": {
                            "x": 411.0,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 90.0,
                            "y": 320.0
                        },
                        "start": {
                            "x": 90.0,
                            "y": 480.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 90.0,
                            "y": 143.0
                        },
                        "control2": {
                            "x": 234.0,
                            "y": 0.0
                        },
                        "end": {
                            "x": 411.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 90.0,
                            "y": 320.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 500.0,
                            "y": 0.0
                        },
                        "control2": {
                            "x": 579.0,
                            "y": 36.0
                        },
                        "end": {
                            "x": 637.0,
                            "y": 94.0
                        },
                        "start": {
                            "x": 411.0,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                }
            ],
            "leftBearing": 40.0,
            "rightBearing": 20.0,
            "width": 707
        },
        "163": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 442.0,
                            "y": 434.0
                        },
                        "start": {
                            "x": 44.0,
                            "y": 434.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 515.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 0.0,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 417.0,
                            "y": 78.0
                        },
                        "control2": {
                            "x": 405.0,
                            "y": 61.0
                        },
                        "end": {
                            "x": 390.0,
                            "y": 47.0
                        },
                        "start": {
                            "x": 425.0,
                            "y": 97.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 361.0,
                            "y": 18.0
                        },
                        "control2": {
                            "x": 320.0,
                            "y": 0.0
                        },
                        "end": {
                            "x": 276.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 390.0,
                            "y": 47.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 186.0,
                            "y": 0.0
                        },
                        "control2": {
                            "x": 114.0,
                            "y": 72.0
                        },
                        "end": {
                            "x": 114.0,
                            "y": 162.0
                        },
                        "start": {
                            "x": 276.0,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 114.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 114.0,
                            "y": 162.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": -50.0,
            "rightBearing": -48.0,
            "width": 517
        },
        "165": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 513.0,
                            "y": 434.0
                        },
                        "start": {
                            "x": 115.0,
                            "y": 434.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 319.0,
                            "y": 400.0
                        },
                        "start": {
                            "x": 319.0,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 319.0,
                            "y": 400.0
                        },
                        "start": {
                            "x": 30.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 609.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 319.0,
                            "y": 400.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": -20.0,
            "rightBearing": -20.0,
            "width": 639
        },
        "166": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 110.0,
                            "y": 1000.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 60.0,
            "width": 220
        },
        "167": {
            "glyph": [
                {
                    "points": {
                        "control1": {
                            "x": 80.0,
                            "y": 768.0
                        },
                        "control2": {
                            "x": 94.0,
                            "y": 789.0
                        },
                        "end": {
                            "x": 112.0,
                            "y": 807.0
                        },
                        "start": {
                            "x": 70.0,
                            "y": 744.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 148.0,
                            "y": 843.0
                        },
                        "control2": {
                            "x": 198.0,
                            "y": 865.0
                        },
                        "end": {
                            "x": 253.0,
                            "y": 865.0
                        },
                        "start": {
                            "x": 112.0,
                            "y": 807.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 362.0,
                            "y": 865.0
                        },
                        "control2": {
                            "x": 450.0,
                            "y": 776.0
                        },
                        "end": {
                            "x": 450.0,
                            "y": 667.0
                        },
                        "start": {
                            "x": 253.0,
                            "y": 865.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 450.0,
                            "y": 613.0
                        },
                        "control2": {
                            "x": 429.7685546875,
                            "y": 555.943359375
                        },
                        "end": {
                            "x": 379.0,
                            "y": 519.0
                        },
                        "start": {
                            "x": 450.0,
                            "y": 667.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 155.0,
                            "y": 356.0
                        },
                        "start": {
                            "x": 379.0,
                            "y": 519.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 118.0,
                            "y": 329.0
                        },
                        "control2": {
                            "x": 88.0,
                            "y": 272.0
                        },
                        "end": {
                            "x": 88.0,
                            "y": 227.0
                        },
                        "start": {
                            "x": 155.0,
                            "y": 356.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 88.0,
                            "y": 138.0
                        },
                        "control2": {
                            "x": 160.0,
                            "y": 65.0
                        },
                        "end": {
                            "x": 250.0,
                            "y": 65.0
                        },
                        "start": {
                            "x": 88.0,
                            "y": 227.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 294.0,
                            "y": 65.0
                        },
                        "control2": {
                            "x": 335.0,
                            "y": 83.0
                        },
                        "end": {
                            "x": 364.0,
                            "y": 112.0
                        },
                        "start": {
                            "x": 250.0,
                            "y": 65.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 379.0,
                            "y": 126.0
                        },
                        "control2": {
                            "x": 391.0,
                            "y": 143.0
                        },
                        "end": {
                            "x": 399.0,
                            "y": 162.0
                        },
                        "start": {
                            "x": 364.0,
                            "y": 112.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 80.0,
                            "y": 636.0
                        },
                        "control2": {
                            "x": 94.0,
                            "y": 657.0
                        },
                        "end": {
                            "x": 112.0,
                            "y": 675.0
                        },
                        "start": {
                            "x": 70.0,
                            "y": 612.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 148.0,
                            "y": 711.0
                        },
                        "control2": {
                            "x": 198.0,
                            "y": 733.0
                        },
                        "end": {
                            "x": 253.0,
                            "y": 733.0
                        },
                        "start": {
                            "x": 112.0,
                            "y": 675.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 362.0,
                            "y": 733.0
                        },
                        "control2": {
                            "x": 450.0,
                            "y": 644.0
                        },
                        "end": {
                            "x": 450.0,
                            "y": 535.0
                        },
                        "start": {
                            "x": 253.0,
                            "y": 733.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 450.0,
                            "y": 481.0
                        },
                        "control2": {
                            "x": 429.7685546875,
                            "y": 423.943359375
                        },
                        "end": {
                            "x": 379.0,
                            "y": 387.0
                        },
                        "start": {
                            "x": 450.0,
                            "y": 535.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 155.0,
                            "y": 224.0
                        },
                        "start": {
                            "x": 379.0,
                            "y": 387.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 118.0,
                            "y": 197.0
                        },
                        "control2": {
                            "x": 88.0,
                            "y": 140.0
                        },
                        "end": {
                            "x": 88.0,
                            "y": 95.0
                        },
                        "start": {
                            "x": 155.0,
                            "y": 224.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 88.0,
                            "y": 6.0
                        },
                        "control2": {
                            "x": 160.0,
                            "y": -67.0
                        },
                        "end": {
                            "x": 250.0,
                            "y": -67.0
                        },
                        "start": {
                            "x": 88.0,
                            "y": 95.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 294.0,
                            "y": -67.0
                        },
                        "control2": {
                            "x": 335.0,
                            "y": -49.0
                        },
                        "end": {
                            "x": 364.0,
                            "y": -20.0
                        },
                        "start": {
                            "x": 250.0,
                            "y": -67.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 379.0,
                            "y": -6.0
                        },
                        "control2": {
                            "x": 391.0,
                            "y": 11.0
                        },
                        "end": {
                            "x": 399.0,
                            "y": 30.0
                        },
                        "start": {
                            "x": 364.0,
                            "y": -20.0
                        }
                    },
                    "type": "C"
                }
            ],
            "leftBearing": 20.0,
            "rightBearing": 20.0,
            "width": 520
        },
        "168": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 263.0,
                            "y": -64.0
                        },
                        "start": {
                            "x": 263.0,
                            "y": -65.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 110.0,
                            "y": -64.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": -65.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 61.0,
            "width": 374
        },
        "169": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 411.0,
                            "y": 863.0
                        },
                        "start": {
                            "x": 411.0,
                            "y": 799.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 411.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 411.0,
                            "y": -64.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 579.0,
                            "y": 764.0
                        },
                        "control2": {
                            "x": 500.0,
                            "y": 800.0
                        },
                        "end": {
                            "x": 411.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 637.0,
                            "y": 706.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 234.0,
                            "y": 800.0
                        },
                        "control2": {
                            "x": 90.0,
                            "y": 657.0
                        },
                        "end": {
                            "x": 90.0,
                            "y": 480.0
                        },
                        "start": {
                            "x": 411.0,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 90.0,
                            "y": 320.0
                        },
                        "start": {
                            "x": 90.0,
                            "y": 480.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 90.0,
                            "y": 143.0
                        },
                        "control2": {
                            "x": 234.0,
                            "y": 0.0
                        },
                        "end": {
                            "x": 411.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 90.0,
                            "y": 320.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 500.0,
                            "y": 0.0
                        },
                        "control2": {
                            "x": 579.0,
                            "y": 36.0
                        },
                        "end": {
                            "x": 637.0,
                            "y": 94.0
                        },
                        "start": {
                            "x": 411.0,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                }
            ],
            "leftBearing": 40.0,
            "rightBearing": 20.0,
            "width": 707
        },
        "170": {
            "glyph": [
                {
                    "points": {
                        "control1": {
                            "x": 478.0,
                            "y": 488.0
                        },
                        "control2": {
                            "x": 308.0,
                            "y": 0.0
                        },
                        "end": {
                            "x": 308.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 587.0,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 30.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 308.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 492.0,
                            "y": 528.0
                        },
                        "start": {
                            "x": 125.0,
                            "y": 528.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": -20.0,
            "rightBearing": -20.0,
            "width": 617
        },
        "171": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 300.0,
                            "y": 584.0
                        },
                        "start": {
                            "x": 466.0,
                            "y": 802.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 466.0,
                            "y": 366.0
                        },
                        "start": {
                            "x": 300.0,
                            "y": 584.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 110.0,
                            "y": 584.0
                        },
                        "start": {
                            "x": 276.0,
                            "y": 802.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 276.0,
                            "y": 366.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 584.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 60.0,
            "width": 576
        },
        "172": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 373.0,
                            "y": 545.0
                        },
                        "start": {
                            "x": 373.0,
                            "y": 472.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 373.0,
                            "y": 472.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 472.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 60.0,
            "width": 483
        },
        "173": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 373.0,
                            "y": 472.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 472.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 60.0,
            "width": 483
        },
        "174": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 287.0,
                            "y": -2.0
                        },
                        "start": {
                            "x": 287.0,
                            "y": -66.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 287.0,
                            "y": 864.0
                        },
                        "start": {
                            "x": 287.0,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 288.9912109375,
                            "y": 400.0
                        },
                        "start": {
                            "x": 489.9912109375,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 109.9912109375,
                            "y": 0.0
                        },
                        "start": {
                            "x": 109.9912109375,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 311.9912109375,
                            "y": 0.0
                        },
                        "start": {
                            "x": 109.9912109375,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 422.9912109375,
                            "y": 0.0
                        },
                        "control2": {
                            "x": 511.0,
                            "y": 91.0
                        },
                        "end": {
                            "x": 509.9912109375,
                            "y": 202.0
                        },
                        "start": {
                            "x": 311.9912109375,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 508.9912109375,
                            "y": 312.0
                        },
                        "control2": {
                            "x": 419.9912109375,
                            "y": 400.0
                        },
                        "end": {
                            "x": 309.9912109375,
                            "y": 400.0
                        },
                        "start": {
                            "x": 509.9912109375,
                            "y": 202.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 309.9912109375,
                            "y": 400.0
                        },
                        "control2": {
                            "x": 187.9912109375,
                            "y": 400.0
                        },
                        "end": {
                            "x": 109.9912109375,
                            "y": 400.0
                        },
                        "start": {
                            "x": 309.9912109375,
                            "y": 400.0
                        }
                    },
                    "type": "C"
                }
            ],
            "leftBearing": 59.991210937999995,
            "rightBearing": 20.000202958291766,
            "width": 580
        },
        "175": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 373.0,
                            "y": -66.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": -66.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 60.0,
            "width": 483
        },
        "176": {
            "glyph": [
                {
                    "points": {
                        "control1": {
                            "x": 110.0,
                            "y": 58.0
                        },
                        "control2": {
                            "x": 168.0,
                            "y": 0.0
                        },
                        "end": {
                            "x": 240.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 130.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 312.0,
                            "y": 0.0
                        },
                        "control2": {
                            "x": 370.0,
                            "y": 58.0
                        },
                        "end": {
                            "x": 370.0,
                            "y": 130.0
                        },
                        "start": {
                            "x": 240.0,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 370.0,
                            "y": 202.0
                        },
                        "control2": {
                            "x": 312.0,
                            "y": 260.0
                        },
                        "end": {
                            "x": 240.0,
                            "y": 260.0
                        },
                        "start": {
                            "x": 370.0,
                            "y": 130.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 168.0,
                            "y": 260.0
                        },
                        "control2": {
                            "x": 110.0,
                            "y": 202.0
                        },
                        "end": {
                            "x": 110.0,
                            "y": 130.0
                        },
                        "start": {
                            "x": 240.0,
                            "y": 260.0
                        }
                    },
                    "type": "C"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 60.0,
            "width": 480
        },
        "177": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 373.0,
                            "y": 672.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 672.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 110.0,
                            "y": 472.0
                        },
                        "start": {
                            "x": 373.0,
                            "y": 472.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 241.5,
                            "y": 340.5
                        },
                        "start": {
                            "x": 241.5,
                            "y": 603.5
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 60.0,
            "width": 483
        },
        "178": {
            "glyph": [
                {
                    "points": {
                        "control1": {
                            "x": 136.0,
                            "y": 78.0
                        },
                        "control2": {
                            "x": 148.0,
                            "y": 61.0
                        },
                        "end": {
                            "x": 163.0,
                            "y": 47.0
                        },
                        "start": {
                            "x": 128.0,
                            "y": 97.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 192.0,
                            "y": 18.0
                        },
                        "control2": {
                            "x": 232.0,
                            "y": 0.0
                        },
                        "end": {
                            "x": 276.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 163.0,
                            "y": 47.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 365.0,
                            "y": 0.0
                        },
                        "control2": {
                            "x": 439.0,
                            "y": 72.0
                        },
                        "end": {
                            "x": 439.0,
                            "y": 162.0
                        },
                        "start": {
                            "x": 276.0,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 439.0,
                            "y": 184.0
                        },
                        "control2": {
                            "x": 431.0,
                            "y": 210.0
                        },
                        "end": {
                            "x": 419.0,
                            "y": 233.0
                        },
                        "start": {
                            "x": 439.0,
                            "y": 162.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 76.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 419.0,
                            "y": 233.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 466.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 76.0,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 26.0,
            "rightBearing": 34.0,
            "width": 550
        },
        "179": {
            "glyph": [
                {
                    "points": {
                        "control1": {
                            "x": 93.0,
                            "y": 80.0
                        },
                        "control2": {
                            "x": 173.0,
                            "y": 0.0
                        },
                        "end": {
                            "x": 272.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 92.0,
                            "y": 178.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 371.0,
                            "y": 0.0
                        },
                        "control2": {
                            "x": 452.0,
                            "y": 81.0
                        },
                        "end": {
                            "x": 452.0,
                            "y": 180.0
                        },
                        "start": {
                            "x": 272.0,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 452.0,
                            "y": 279.0
                        },
                        "control2": {
                            "x": 372.0048828125,
                            "y": 361.0
                        },
                        "end": {
                            "x": 272.0,
                            "y": 361.0
                        },
                        "start": {
                            "x": 452.0,
                            "y": 180.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 393.037109375,
                            "y": 361.0
                        },
                        "control2": {
                            "x": 495.0,
                            "y": 462.0
                        },
                        "end": {
                            "x": 495.0,
                            "y": 582.0
                        },
                        "start": {
                            "x": 272.0,
                            "y": 361.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 495.0,
                            "y": 703.0
                        },
                        "control2": {
                            "x": 396.0,
                            "y": 800.0
                        },
                        "end": {
                            "x": 275.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 495.0,
                            "y": 582.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 153.0,
                            "y": 800.0
                        },
                        "control2": {
                            "x": 55.0,
                            "y": 703.0
                        },
                        "end": {
                            "x": 55.0,
                            "y": 582.0
                        },
                        "start": {
                            "x": 275.0,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                }
            ],
            "leftBearing": 5.0,
            "rightBearing": 5.0,
            "width": 550
        },
        "180": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 110.0,
                            "y": -64.0
                        },
                        "start": {
                            "x": 262.0,
                            "y": -171.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 60.0,
            "width": 372
        },
        "181": {
            "glyph": [
                {
                    "points": {
                        "control1": {
                            "x": 730.0,
                            "y": 185.0
                        },
                        "control2": {
                            "x": 730.0,
                            "y": 480.0
                        },
                        "end": {
                            "x": 730.0,
                            "y": 480.0
                        },
                        "start": {
                            "x": 730.0,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 730.0,
                            "y": 657.0
                        },
                        "control2": {
                            "x": 587.0,
                            "y": 800.0
                        },
                        "end": {
                            "x": 410.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 730.0,
                            "y": 480.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 233.0,
                            "y": 800.0
                        },
                        "control2": {
                            "x": 90.0,
                            "y": 656.8974609375
                        },
                        "end": {
                            "x": 90.0,
                            "y": 480.0
                        },
                        "start": {
                            "x": 410.0,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 90.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 90.0,
                            "y": 480.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 40.0,
            "rightBearing": 40.0,
            "width": 820
        },
        "182": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 466.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 466.0,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 350.0068359375,
                            "y": 480.0
                        },
                        "start": {
                            "x": 588.0068359375,
                            "y": 480.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 218.0068359375,
                            "y": 480.0
                        },
                        "control2": {
                            "x": 110.9990234375,
                            "y": 374.0
                        },
                        "end": {
                            "x": 110.0068359375,
                            "y": 242.0
                        },
                        "start": {
                            "x": 350.0068359375,
                            "y": 480.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 109.0068359375,
                            "y": 109.0
                        },
                        "control2": {
                            "x": 215.0068359375,
                            "y": 1.0
                        },
                        "end": {
                            "x": 348.0068359375,
                            "y": 0.0
                        },
                        "start": {
                            "x": 110.0068359375,
                            "y": 242.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 588.0068359375,
                            "y": 0.0
                        },
                        "start": {
                            "x": 348.0068359375,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 588.0068359375,
                            "y": 800.0
                        },
                        "start": {
                            "x": 588.0068359375,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 59.999818407762206,
            "rightBearing": -0.006835937999994712,
            "width": 638
        },
        "184": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 110.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 951.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 60.0,
            "width": 220
        },
        "185": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 335.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 335.0,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 334.0,
                            "y": 98.0
                        },
                        "control2": {
                            "x": 254.0,
                            "y": 178.0
                        },
                        "end": {
                            "x": 155.0,
                            "y": 178.0
                        },
                        "start": {
                            "x": 335.0,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                }
            ],
            "leftBearing": 105.0,
            "rightBearing": 165.0,
            "width": 550
        },
        "186": {
            "glyph": [
                {
                    "points": {
                        "control1": {
                            "x": 110.0,
                            "y": 58.0
                        },
                        "control2": {
                            "x": 168.0,
                            "y": 0.0
                        },
                        "end": {
                            "x": 240.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 130.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 312.0,
                            "y": 0.0
                        },
                        "control2": {
                            "x": 370.0,
                            "y": 58.0
                        },
                        "end": {
                            "x": 370.0,
                            "y": 130.0
                        },
                        "start": {
                            "x": 240.0,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 370.0,
                            "y": 202.0
                        },
                        "control2": {
                            "x": 312.0,
                            "y": 260.0
                        },
                        "end": {
                            "x": 240.0,
                            "y": 260.0
                        },
                        "start": {
                            "x": 370.0,
                            "y": 130.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 168.0,
                            "y": 260.0
                        },
                        "control2": {
                            "x": 110.0,
                            "y": 202.0
                        },
                        "end": {
                            "x": 110.0,
                            "y": 130.0
                        },
                        "start": {
                            "x": 240.0,
                            "y": 260.0
                        }
                    },
                    "type": "C"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 60.0,
            "width": 480
        },
        "187": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 276.0,
                            "y": 584.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 802.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 110.0,
                            "y": 366.0
                        },
                        "start": {
                            "x": 276.0,
                            "y": 584.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 466.0,
                            "y": 584.0
                        },
                        "start": {
                            "x": 300.0,
                            "y": 802.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 300.0,
                            "y": 366.0
                        },
                        "start": {
                            "x": 466.0,
                            "y": 584.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 60.0,
            "width": 576
        },
        "188": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 16.0,
                            "y": 572.0
                        },
                        "start": {
                            "x": 376.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 525.0,
                            "y": 572.0
                        },
                        "start": {
                            "x": 16.0,
                            "y": 572.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 376.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 376.0,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 265.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 265.0,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 264.0,
                            "y": 98.0
                        },
                        "control2": {
                            "x": 184.0,
                            "y": 178.0
                        },
                        "end": {
                            "x": 85.0,
                            "y": 178.0
                        },
                        "start": {
                            "x": 265.0,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                }
            ],
            "leftBearing": -34.0,
            "rightBearing": -25.0,
            "width": 550
        },
        "189": {
            "glyph": [
                {
                    "points": {
                        "control1": {
                            "x": 136.0,
                            "y": 78.0
                        },
                        "control2": {
                            "x": 148.0,
                            "y": 61.0
                        },
                        "end": {
                            "x": 163.0,
                            "y": 47.0
                        },
                        "start": {
                            "x": 128.0,
                            "y": 97.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 192.0,
                            "y": 18.0
                        },
                        "control2": {
                            "x": 232.0,
                            "y": 0.0
                        },
                        "end": {
                            "x": 276.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 163.0,
                            "y": 47.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 365.0,
                            "y": 0.0
                        },
                        "control2": {
                            "x": 439.0,
                            "y": 72.0
                        },
                        "end": {
                            "x": 439.0,
                            "y": 162.0
                        },
                        "start": {
                            "x": 276.0,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 439.0,
                            "y": 184.0
                        },
                        "control2": {
                            "x": 431.0,
                            "y": 210.0
                        },
                        "end": {
                            "x": 419.0,
                            "y": 233.0
                        },
                        "start": {
                            "x": 439.0,
                            "y": 162.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 76.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 419.0,
                            "y": 233.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 466.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 76.0,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 335.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 335.0,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 334.0,
                            "y": 98.0
                        },
                        "control2": {
                            "x": 254.0,
                            "y": 178.0
                        },
                        "end": {
                            "x": 155.0,
                            "y": 178.0
                        },
                        "start": {
                            "x": 335.0,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                }
            ],
            "leftBearing": 26.0,
            "rightBearing": 34.0,
            "width": 550
        },
        "190": {
            "glyph": [
                {
                    "points": {
                        "control1": {
                            "x": 93.0,
                            "y": 80.0
                        },
                        "control2": {
                            "x": 173.0,
                            "y": 0.0
                        },
                        "end": {
                            "x": 272.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 92.0,
                            "y": 178.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 371.0,
                            "y": 0.0
                        },
                        "control2": {
                            "x": 452.0,
                            "y": 81.0
                        },
                        "end": {
                            "x": 452.0,
                            "y": 180.0
                        },
                        "start": {
                            "x": 272.0,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 452.0,
                            "y": 279.0
                        },
                        "control2": {
                            "x": 372.0048828125,
                            "y": 361.0
                        },
                        "end": {
                            "x": 272.0,
                            "y": 361.0
                        },
                        "start": {
                            "x": 452.0,
                            "y": 180.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 393.037109375,
                            "y": 361.0
                        },
                        "control2": {
                            "x": 495.0,
                            "y": 462.0
                        },
                        "end": {
                            "x": 495.0,
                            "y": 582.0
                        },
                        "start": {
                            "x": 272.0,
                            "y": 361.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 495.0,
                            "y": 703.0
                        },
                        "control2": {
                            "x": 396.0,
                            "y": 800.0
                        },
                        "end": {
                            "x": 275.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 495.0,
                            "y": 582.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 153.0,
                            "y": 800.0
                        },
                        "control2": {
                            "x": 55.0,
                            "y": 703.0
                        },
                        "end": {
                            "x": 55.0,
                            "y": 582.0
                        },
                        "start": {
                            "x": 275.0,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 16.0,
                            "y": 572.0
                        },
                        "start": {
                            "x": 376.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 525.0,
                            "y": 572.0
                        },
                        "start": {
                            "x": 16.0,
                            "y": 572.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 376.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 376.0,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": -34.0,
            "rightBearing": -25.0,
            "width": 550
        },
        "191": {
            "glyph": [
                {
                    "points": {
                        "control1": {
                            "x": 413.0,
                            "y": 720.0
                        },
                        "control2": {
                            "x": 401.0,
                            "y": 738.0
                        },
                        "end": {
                            "x": 386.0,
                            "y": 752.0
                        },
                        "start": {
                            "x": 421.0,
                            "y": 701.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 357.0,
                            "y": 781.0
                        },
                        "control2": {
                            "x": 316.0,
                            "y": 799.0
                        },
                        "end": {
                            "x": 272.0,
                            "y": 799.0
                        },
                        "start": {
                            "x": 386.0,
                            "y": 752.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 182.0,
                            "y": 799.0
                        },
                        "control2": {
                            "x": 110.0,
                            "y": 726.0
                        },
                        "end": {
                            "x": 110.0,
                            "y": 637.0
                        },
                        "start": {
                            "x": 272.0,
                            "y": 799.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 110.0,
                            "y": 615.0
                        },
                        "control2": {
                            "x": 118.0,
                            "y": 589.0
                        },
                        "end": {
                            "x": 130.0,
                            "y": 566.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 637.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 255.0,
                            "y": 357.0
                        },
                        "start": {
                            "x": 130.0,
                            "y": 566.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 294.3486328125,
                            "y": 291.208984375
                        },
                        "control2": {
                            "x": 302.0,
                            "y": 243.0
                        },
                        "end": {
                            "x": 302.0,
                            "y": 188.0
                        },
                        "start": {
                            "x": 255.0,
                            "y": 357.0
                        }
                    },
                    "type": "C"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 60.0,
            "width": 531
        },
        "192": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 381.0,
                            "y": -64.0
                        },
                        "start": {
                            "x": 228.0,
                            "y": -171.099609375
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 477.999804687,
                            "y": 488.0
                        },
                        "control2": {
                            "x": 307.999804687,
                            "y": 0.0
                        },
                        "end": {
                            "x": 307.999804687,
                            "y": 0.0
                        },
                        "start": {
                            "x": 586.999804687,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 29.999804687,
                            "y": 800.0
                        },
                        "start": {
                            "x": 307.999804687,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 491.999804687,
                            "y": 528.0
                        },
                        "start": {
                            "x": 124.999804687,
                            "y": 528.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": -20.0,
            "rightBearing": -20.0,
            "width": 617
        },
        "193": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 226.0,
                            "y": -64.0
                        },
                        "start": {
                            "x": 378.0,
                            "y": -171.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 477.999804687,
                            "y": 488.0
                        },
                        "control2": {
                            "x": 307.999804687,
                            "y": 0.0
                        },
                        "end": {
                            "x": 307.999804687,
                            "y": 0.0
                        },
                        "start": {
                            "x": 586.999804687,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 29.999804687,
                            "y": 800.0
                        },
                        "start": {
                            "x": 307.999804687,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 491.999804687,
                            "y": 528.0
                        },
                        "start": {
                            "x": 124.999804687,
                            "y": 528.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": -20.0,
            "rightBearing": -20.0,
            "width": 617
        },
        "194": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 304.9998,
                            "y": -171.099609375
                        },
                        "start": {
                            "x": 151.9998,
                            "y": -64.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 457.9998,
                            "y": -64.0
                        },
                        "start": {
                            "x": 304.9998,
                            "y": -171.099609375
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 477.999804687,
                            "y": 488.0
                        },
                        "control2": {
                            "x": 307.999804687,
                            "y": 0.0
                        },
                        "end": {
                            "x": 307.999804687,
                            "y": 0.0
                        },
                        "start": {
                            "x": 586.999804687,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 29.999804687,
                            "y": 800.0
                        },
                        "start": {
                            "x": 307.999804687,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 491.999804687,
                            "y": 528.0
                        },
                        "start": {
                            "x": 124.999804687,
                            "y": 528.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": -20.0,
            "rightBearing": -20.0,
            "width": 617
        },
        "195": {
            "glyph": [
                {
                    "points": {
                        "control1": {
                            "x": 417.103315625,
                            "y": -71.0498046875
                        },
                        "control2": {
                            "x": 394.829878125,
                            "y": -62.564453125
                        },
                        "end": {
                            "x": 372.5554640625,
                            "y": -62.564453125
                        },
                        "start": {
                            "x": 434.07401875,
                            "y": -88.0205078125
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 350.28105,
                            "y": -62.564453125
                        },
                        "control2": {
                            "x": 328.0076125,
                            "y": -71.0498046875
                        },
                        "end": {
                            "x": 311.036909375,
                            "y": -88.0205078125
                        },
                        "start": {
                            "x": 372.5554640625,
                            "y": -62.564453125
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 294.06620625,
                            "y": -104.9912109375
                        },
                        "control2": {
                            "x": 271.79276875,
                            "y": -113.4765625
                        },
                        "end": {
                            "x": 249.5183546875,
                            "y": -113.4765625
                        },
                        "start": {
                            "x": 311.036909375,
                            "y": -88.0205078125
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 227.243940625,
                            "y": -113.4765625
                        },
                        "control2": {
                            "x": 204.970503125,
                            "y": -104.9912109375
                        },
                        "end": {
                            "x": 187.9998,
                            "y": -88.0205078125
                        },
                        "start": {
                            "x": 249.5183546875,
                            "y": -113.4765625
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 477.999804687,
                            "y": 488.0
                        },
                        "control2": {
                            "x": 307.999804687,
                            "y": 0.0
                        },
                        "end": {
                            "x": 307.999804687,
                            "y": 0.0
                        },
                        "start": {
                            "x": 586.999804687,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 29.999804687,
                            "y": 800.0
                        },
                        "start": {
                            "x": 307.999804687,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 491.999804687,
                            "y": 528.0
                        },
                        "start": {
                            "x": 124.999804687,
                            "y": 528.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": -20.0,
            "rightBearing": -20.0,
            "width": 617
        },
        "196": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 380.0,
                            "y": -64.0
                        },
                        "start": {
                            "x": 380.0,
                            "y": -65.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 227.0,
                            "y": -64.0
                        },
                        "start": {
                            "x": 227.0,
                            "y": -65.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 477.999804687,
                            "y": 488.0
                        },
                        "control2": {
                            "x": 307.999804687,
                            "y": 0.0
                        },
                        "end": {
                            "x": 307.999804687,
                            "y": 0.0
                        },
                        "start": {
                            "x": 586.999804687,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 29.999804687,
                            "y": 800.0
                        },
                        "start": {
                            "x": 307.999804687,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 491.999804687,
                            "y": 528.0
                        },
                        "start": {
                            "x": 124.999804687,
                            "y": 528.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": -20.0,
            "rightBearing": -20.0,
            "width": 617
        },
        "197": {
            "glyph": [
                {
                    "points": {
                        "control1": {
                            "x": 180.0,
                            "y": -200.0
                        },
                        "control2": {
                            "x": 238.0,
                            "y": -258.0
                        },
                        "end": {
                            "x": 310.0,
                            "y": -258.0
                        },
                        "start": {
                            "x": 180.0,
                            "y": -128.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 382.0,
                            "y": -258.0
                        },
                        "control2": {
                            "x": 440.0,
                            "y": -200.0
                        },
                        "end": {
                            "x": 440.0,
                            "y": -128.0
                        },
                        "start": {
                            "x": 310.0,
                            "y": -258.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 440.0,
                            "y": -56.0
                        },
                        "control2": {
                            "x": 382.0,
                            "y": 2.0
                        },
                        "end": {
                            "x": 310.0,
                            "y": 2.0
                        },
                        "start": {
                            "x": 440.0,
                            "y": -128.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 238.0,
                            "y": 2.0
                        },
                        "control2": {
                            "x": 180.0,
                            "y": -56.0
                        },
                        "end": {
                            "x": 180.0,
                            "y": -128.0
                        },
                        "start": {
                            "x": 310.0,
                            "y": 2.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 477.999804687,
                            "y": 488.0
                        },
                        "control2": {
                            "x": 307.999804687,
                            "y": 0.0
                        },
                        "end": {
                            "x": 307.999804687,
                            "y": 0.0
                        },
                        "start": {
                            "x": 586.999804687,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 29.999804687,
                            "y": 800.0
                        },
                        "start": {
                            "x": 307.999804687,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 491.999804687,
                            "y": 528.0
                        },
                        "start": {
                            "x": 124.999804687,
                            "y": 528.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": -20.0,
            "rightBearing": -20.0,
            "width": 617
        },
        "198": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 867.0,
                            "y": 400.0
                        },
                        "start": {
                            "x": 507.0,
                            "y": 400.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 867.0,
                            "y": 400.0
                        },
                        "start": {
                            "x": 507.0,
                            "y": 400.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 507.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 947.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 507.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 507.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 947.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 507.0,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 41.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 507.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 507.0,
                            "y": 528.0
                        },
                        "start": {
                            "x": 201.0,
                            "y": 526.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": -9.0,
            "rightBearing": 55.0,
            "width": 1052
        },
        "199": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 411.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 411.0,
                            "y": 951.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 579.0,
                            "y": 764.0
                        },
                        "control2": {
                            "x": 500.0,
                            "y": 800.0
                        },
                        "end": {
                            "x": 411.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 637.0,
                            "y": 706.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 234.0,
                            "y": 800.0
                        },
                        "control2": {
                            "x": 90.0,
                            "y": 657.0
                        },
                        "end": {
                            "x": 90.0,
                            "y": 480.0
                        },
                        "start": {
                            "x": 411.0,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 90.0,
                            "y": 320.0
                        },
                        "start": {
                            "x": 90.0,
                            "y": 480.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 90.0,
                            "y": 143.0
                        },
                        "control2": {
                            "x": 234.0,
                            "y": 0.0
                        },
                        "end": {
                            "x": 411.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 90.0,
                            "y": 320.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 500.0,
                            "y": 0.0
                        },
                        "control2": {
                            "x": 579.0,
                            "y": 36.0
                        },
                        "end": {
                            "x": 637.0,
                            "y": 94.0
                        },
                        "start": {
                            "x": 411.0,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                }
            ],
            "leftBearing": 40.0,
            "rightBearing": 20.0,
            "width": 707
        },
        "200": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 404.75,
                            "y": -64.0
                        },
                        "start": {
                            "x": 251.75,
                            "y": -171.099609375
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 470.000390625,
                            "y": 400.0
                        },
                        "start": {
                            "x": 110.000390625,
                            "y": 400.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 470.000390625,
                            "y": 400.0
                        },
                        "start": {
                            "x": 110.000390625,
                            "y": 400.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 110.000390625,
                            "y": 0.0
                        },
                        "start": {
                            "x": 550.000390625,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 110.000390625,
                            "y": 800.0
                        },
                        "start": {
                            "x": 110.000390625,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 550.000390625,
                            "y": 800.0
                        },
                        "start": {
                            "x": 110.000390625,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 10.0,
            "width": 610
        },
        "201": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 262.75,
                            "y": -64.0
                        },
                        "start": {
                            "x": 414.75,
                            "y": -171.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 470.000390625,
                            "y": 400.0
                        },
                        "start": {
                            "x": 110.000390625,
                            "y": 400.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 470.000390625,
                            "y": 400.0
                        },
                        "start": {
                            "x": 110.000390625,
                            "y": 400.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 110.000390625,
                            "y": 0.0
                        },
                        "start": {
                            "x": 550.000390625,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 110.000390625,
                            "y": 800.0
                        },
                        "start": {
                            "x": 110.000390625,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 550.000390625,
                            "y": 800.0
                        },
                        "start": {
                            "x": 110.000390625,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 10.0,
            "width": 610
        },
        "202": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 335.7504,
                            "y": -171.099609375
                        },
                        "start": {
                            "x": 182.7504,
                            "y": -64.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 488.7504,
                            "y": -64.0
                        },
                        "start": {
                            "x": 335.7504,
                            "y": -171.099609375
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 470.000390625,
                            "y": 400.0
                        },
                        "start": {
                            "x": 110.000390625,
                            "y": 400.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 470.000390625,
                            "y": 400.0
                        },
                        "start": {
                            "x": 110.000390625,
                            "y": 400.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 110.000390625,
                            "y": 0.0
                        },
                        "start": {
                            "x": 550.000390625,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 110.000390625,
                            "y": 800.0
                        },
                        "start": {
                            "x": 110.000390625,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 550.000390625,
                            "y": 800.0
                        },
                        "start": {
                            "x": 110.000390625,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 10.0,
            "width": 610
        },
        "203": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 405.75,
                            "y": -61.99996999999996
                        },
                        "start": {
                            "x": 405.75,
                            "y": -62.99996999999996
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 252.75,
                            "y": -61.99996999999996
                        },
                        "start": {
                            "x": 252.75,
                            "y": -62.99996999999996
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 469.999804688,
                            "y": 400.0
                        },
                        "start": {
                            "x": 109.999804688,
                            "y": 400.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 469.999804688,
                            "y": 400.0
                        },
                        "start": {
                            "x": 109.999804688,
                            "y": 400.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 109.999804688,
                            "y": 0.0
                        },
                        "start": {
                            "x": 549.999804688,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 109.999804688,
                            "y": 800.0
                        },
                        "start": {
                            "x": 109.999804688,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 549.999804688,
                            "y": 800.0
                        },
                        "start": {
                            "x": 109.999804688,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 10.0,
            "width": 610
        },
        "204": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 198.4998,
                            "y": -64.0
                        },
                        "start": {
                            "x": 45.49979999999999,
                            "y": -171.099609375
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 123.4998,
                            "y": 800.0
                        },
                        "start": {
                            "x": 123.4998,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": -4.5,
            "rightBearing": -4.5,
            "width": 244
        },
        "205": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 46.0004,
                            "y": -64.0
                        },
                        "start": {
                            "x": 198.0004,
                            "y": -171.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 126.00039,
                            "y": 800.0
                        },
                        "start": {
                            "x": 126.00039,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": -4.0,
            "rightBearing": -4.0,
            "width": 244
        },
        "206": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 122.0,
                            "y": -171.099609375
                        },
                        "start": {
                            "x": -31.0,
                            "y": -64.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 275.0,
                            "y": -64.0
                        },
                        "start": {
                            "x": 122.0,
                            "y": -171.099609375
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 118.99971,
                            "y": 800.0
                        },
                        "start": {
                            "x": 118.99971,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": -81.0,
            "rightBearing": -81.0,
            "width": 244
        },
        "207": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 198.0002,
                            "y": -65.99996999999996
                        },
                        "start": {
                            "x": 198.0002,
                            "y": -66.99996999999996
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 45.00020000000001,
                            "y": -65.99996999999996
                        },
                        "start": {
                            "x": 45.00020000000001,
                            "y": -66.99996999999996
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 133.0002,
                            "y": 800.0
                        },
                        "start": {
                            "x": 133.0002,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": -5.0,
            "rightBearing": -4.0,
            "width": 244
        },
        "208": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 262.5,
                            "y": 400.0
                        },
                        "start": {
                            "x": -0.5,
                            "y": 400.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 132.5,
                            "y": 0.0
                        },
                        "start": {
                            "x": 132.5,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 332.5,
                            "y": 0.0
                        },
                        "start": {
                            "x": 132.5,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 434.5,
                            "y": 0.0
                        },
                        "control2": {
                            "x": 536.5,
                            "y": 39.0
                        },
                        "end": {
                            "x": 614.5,
                            "y": 117.0
                        },
                        "start": {
                            "x": 332.5,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 770.5,
                            "y": 273.0
                        },
                        "control2": {
                            "x": 770.5,
                            "y": 527.0
                        },
                        "end": {
                            "x": 614.5,
                            "y": 683.0
                        },
                        "start": {
                            "x": 614.5,
                            "y": 117.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 536.5,
                            "y": 761.0
                        },
                        "control2": {
                            "x": 432.5,
                            "y": 800.0
                        },
                        "end": {
                            "x": 331.5,
                            "y": 800.0
                        },
                        "start": {
                            "x": 614.5,
                            "y": 683.0
                        }
                    },
                    "type": "C"
                }
            ],
            "leftBearing": -50.5,
            "rightBearing": 19.5,
            "width": 801
        },
        "209": {
            "glyph": [
                {
                    "points": {
                        "control1": {
                            "x": 495.853515625,
                            "y": -71.0498046875
                        },
                        "control2": {
                            "x": 473.580078125,
                            "y": -62.564453125
                        },
                        "end": {
                            "x": 451.3056640625,
                            "y": -62.564453125
                        },
                        "start": {
                            "x": 512.82421875,
                            "y": -88.0205078125
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 429.03125,
                            "y": -62.564453125
                        },
                        "control2": {
                            "x": 406.7578125,
                            "y": -71.0498046875
                        },
                        "end": {
                            "x": 389.787109375,
                            "y": -88.0205078125
                        },
                        "start": {
                            "x": 451.3056640625,
                            "y": -62.564453125
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 372.81640625,
                            "y": -104.9912109375
                        },
                        "control2": {
                            "x": 350.54296875,
                            "y": -113.4765625
                        },
                        "end": {
                            "x": 328.2685546875,
                            "y": -113.4765625
                        },
                        "start": {
                            "x": 389.787109375,
                            "y": -88.0205078125
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 305.994140625,
                            "y": -113.4765625
                        },
                        "control2": {
                            "x": 283.720703125,
                            "y": -104.9912109375
                        },
                        "end": {
                            "x": 266.75,
                            "y": -88.0205078125
                        },
                        "start": {
                            "x": 328.2685546875,
                            "y": -113.4765625
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 670.000390625,
                            "y": 800.0
                        },
                        "start": {
                            "x": 110.000390625,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 110.000390625,
                            "y": 0.0
                        },
                        "start": {
                            "x": 110.000390625,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 670.000390625,
                            "y": 800.0
                        },
                        "start": {
                            "x": 670.000390625,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 60.0,
            "width": 780
        },
        "210": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 545.0,
                            "y": -64.0
                        },
                        "start": {
                            "x": 392.0,
                            "y": -171.099609375
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 248.9999023437,
                            "y": 800.0
                        },
                        "control2": {
                            "x": 69.9999023437,
                            "y": 621.0
                        },
                        "end": {
                            "x": 69.9999023437,
                            "y": 400.0
                        },
                        "start": {
                            "x": 469.9999023437,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 69.9999023437,
                            "y": 179.0
                        },
                        "control2": {
                            "x": 248.9999023437,
                            "y": 0.0
                        },
                        "end": {
                            "x": 469.9999023437,
                            "y": 0.0
                        },
                        "start": {
                            "x": 69.9999023437,
                            "y": 400.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 690.9999023437,
                            "y": 0.0
                        },
                        "control2": {
                            "x": 869.9999023437,
                            "y": 179.0
                        },
                        "end": {
                            "x": 869.9999023437,
                            "y": 400.0
                        },
                        "start": {
                            "x": 469.9999023437,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 869.9999023437,
                            "y": 621.0
                        },
                        "control2": {
                            "x": 690.9999023437,
                            "y": 800.0
                        },
                        "end": {
                            "x": 469.9999023437,
                            "y": 800.0
                        },
                        "start": {
                            "x": 869.9999023437,
                            "y": 400.0
                        }
                    },
                    "type": "C"
                }
            ],
            "leftBearing": 20.0,
            "rightBearing": 20.0,
            "width": 940
        },
        "211": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 390.0,
                            "y": -64.0
                        },
                        "start": {
                            "x": 542.0,
                            "y": -171.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 248.9999023437,
                            "y": 800.0
                        },
                        "control2": {
                            "x": 69.9999023437,
                            "y": 621.0
                        },
                        "end": {
                            "x": 69.9999023437,
                            "y": 400.0
                        },
                        "start": {
                            "x": 469.9999023437,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 69.9999023437,
                            "y": 179.0
                        },
                        "control2": {
                            "x": 248.9999023437,
                            "y": 0.0
                        },
                        "end": {
                            "x": 469.9999023437,
                            "y": 0.0
                        },
                        "start": {
                            "x": 69.9999023437,
                            "y": 400.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 690.9999023437,
                            "y": 0.0
                        },
                        "control2": {
                            "x": 869.9999023437,
                            "y": 179.0
                        },
                        "end": {
                            "x": 869.9999023437,
                            "y": 400.0
                        },
                        "start": {
                            "x": 469.9999023437,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 869.9999023437,
                            "y": 621.0
                        },
                        "control2": {
                            "x": 690.9999023437,
                            "y": 800.0
                        },
                        "end": {
                            "x": 469.9999023437,
                            "y": 800.0
                        },
                        "start": {
                            "x": 869.9999023437,
                            "y": 400.0
                        }
                    },
                    "type": "C"
                }
            ],
            "leftBearing": 20.0,
            "rightBearing": 20.0,
            "width": 940
        },
        "212": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 471.0,
                            "y": -171.099609375
                        },
                        "start": {
                            "x": 318.0,
                            "y": -64.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 624.0,
                            "y": -64.0
                        },
                        "start": {
                            "x": 471.0,
                            "y": -171.099609375
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 248.9999023437,
                            "y": 800.0
                        },
                        "control2": {
                            "x": 69.9999023437,
                            "y": 621.0
                        },
                        "end": {
                            "x": 69.9999023437,
                            "y": 400.0
                        },
                        "start": {
                            "x": 469.9999023437,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 69.9999023437,
                            "y": 179.0
                        },
                        "control2": {
                            "x": 248.9999023437,
                            "y": 0.0
                        },
                        "end": {
                            "x": 469.9999023437,
                            "y": 0.0
                        },
                        "start": {
                            "x": 69.9999023437,
                            "y": 400.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 690.9999023437,
                            "y": 0.0
                        },
                        "control2": {
                            "x": 869.9999023437,
                            "y": 179.0
                        },
                        "end": {
                            "x": 869.9999023437,
                            "y": 400.0
                        },
                        "start": {
                            "x": 469.9999023437,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 869.9999023437,
                            "y": 621.0
                        },
                        "control2": {
                            "x": 690.9999023437,
                            "y": 800.0
                        },
                        "end": {
                            "x": 469.9999023437,
                            "y": 800.0
                        },
                        "start": {
                            "x": 869.9999023437,
                            "y": 400.0
                        }
                    },
                    "type": "C"
                }
            ],
            "leftBearing": 20.0,
            "rightBearing": 20.0,
            "width": 940
        },
        "213": {
            "glyph": [
                {
                    "points": {
                        "control1": {
                            "x": 579.103515625,
                            "y": -71.0498046875
                        },
                        "control2": {
                            "x": 556.830078125,
                            "y": -62.564453125
                        },
                        "end": {
                            "x": 534.5556640625,
                            "y": -62.564453125
                        },
                        "start": {
                            "x": 596.07421875,
                            "y": -88.0205078125
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 512.28125,
                            "y": -62.564453125
                        },
                        "control2": {
                            "x": 490.0078125,
                            "y": -71.0498046875
                        },
                        "end": {
                            "x": 473.037109375,
                            "y": -88.0205078125
                        },
                        "start": {
                            "x": 534.5556640625,
                            "y": -62.564453125
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 456.06640625,
                            "y": -104.9912109375
                        },
                        "control2": {
                            "x": 433.79296875,
                            "y": -113.4765625
                        },
                        "end": {
                            "x": 411.5185546875,
                            "y": -113.4765625
                        },
                        "start": {
                            "x": 473.037109375,
                            "y": -88.0205078125
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 389.244140625,
                            "y": -113.4765625
                        },
                        "control2": {
                            "x": 366.970703125,
                            "y": -104.9912109375
                        },
                        "end": {
                            "x": 350.0,
                            "y": -88.0205078125
                        },
                        "start": {
                            "x": 411.5185546875,
                            "y": -113.4765625
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 248.9999023437,
                            "y": 800.0
                        },
                        "control2": {
                            "x": 69.9999023437,
                            "y": 621.0
                        },
                        "end": {
                            "x": 69.9999023437,
                            "y": 400.0
                        },
                        "start": {
                            "x": 469.9999023437,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 69.9999023437,
                            "y": 179.0
                        },
                        "control2": {
                            "x": 248.9999023437,
                            "y": 0.0
                        },
                        "end": {
                            "x": 469.9999023437,
                            "y": 0.0
                        },
                        "start": {
                            "x": 69.9999023437,
                            "y": 400.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 690.9999023437,
                            "y": 0.0
                        },
                        "control2": {
                            "x": 869.9999023437,
                            "y": 179.0
                        },
                        "end": {
                            "x": 869.9999023437,
                            "y": 400.0
                        },
                        "start": {
                            "x": 469.9999023437,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 869.9999023437,
                            "y": 621.0
                        },
                        "control2": {
                            "x": 690.9999023437,
                            "y": 800.0
                        },
                        "end": {
                            "x": 469.9999023437,
                            "y": 800.0
                        },
                        "start": {
                            "x": 869.9999023437,
                            "y": 400.0
                        }
                    },
                    "type": "C"
                }
            ],
            "leftBearing": 20.0,
            "rightBearing": 20.0,
            "width": 940
        },
        "214": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 543.0,
                            "y": -64.0
                        },
                        "start": {
                            "x": 543.0,
                            "y": -65.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 390.0,
                            "y": -64.0
                        },
                        "start": {
                            "x": 390.0,
                            "y": -65.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 248.9999023437,
                            "y": 800.0
                        },
                        "control2": {
                            "x": 69.9999023437,
                            "y": 621.0
                        },
                        "end": {
                            "x": 69.9999023437,
                            "y": 400.0
                        },
                        "start": {
                            "x": 469.9999023437,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 69.9999023437,
                            "y": 179.0
                        },
                        "control2": {
                            "x": 248.9999023437,
                            "y": 0.0
                        },
                        "end": {
                            "x": 469.9999023437,
                            "y": 0.0
                        },
                        "start": {
                            "x": 69.9999023437,
                            "y": 400.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 690.9999023437,
                            "y": 0.0
                        },
                        "control2": {
                            "x": 869.9999023437,
                            "y": 179.0
                        },
                        "end": {
                            "x": 869.9999023437,
                            "y": 400.0
                        },
                        "start": {
                            "x": 469.9999023437,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 869.9999023437,
                            "y": 621.0
                        },
                        "control2": {
                            "x": 690.9999023437,
                            "y": 800.0
                        },
                        "end": {
                            "x": 469.9999023437,
                            "y": 800.0
                        },
                        "start": {
                            "x": 869.9999023437,
                            "y": 400.0
                        }
                    },
                    "type": "C"
                }
            ],
            "leftBearing": 20.0,
            "rightBearing": 20.0,
            "width": 940
        },
        "215": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": -353.86232600000005,
                            "y": 247.28713399999992
                        },
                        "start": {
                            "x": -167.893185,
                            "y": 61.31799299999989
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": -353.86232600000005,
                            "y": 61.31799299999989
                        },
                        "start": {
                            "x": -167.893185,
                            "y": 247.28713399999992
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 59.03125,
            "width": 405
        },
        "216": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 350.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 605.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 249.0,
                            "y": 800.0
                        },
                        "control2": {
                            "x": 70.0,
                            "y": 621.0
                        },
                        "end": {
                            "x": 70.0,
                            "y": 400.0
                        },
                        "start": {
                            "x": 470.0,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 70.0,
                            "y": 179.0
                        },
                        "control2": {
                            "x": 249.0,
                            "y": 0.0
                        },
                        "end": {
                            "x": 470.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 70.0,
                            "y": 400.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 691.0,
                            "y": 0.0
                        },
                        "control2": {
                            "x": 870.0,
                            "y": 179.0
                        },
                        "end": {
                            "x": 870.0,
                            "y": 400.0
                        },
                        "start": {
                            "x": 470.0,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 870.0,
                            "y": 621.0
                        },
                        "control2": {
                            "x": 691.0,
                            "y": 800.0
                        },
                        "end": {
                            "x": 470.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 870.0,
                            "y": 400.0
                        }
                    },
                    "type": "C"
                }
            ],
            "leftBearing": 20.0,
            "rightBearing": 20.0,
            "width": 940
        },
        "217": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 488.75,
                            "y": -64.0
                        },
                        "start": {
                            "x": 335.75,
                            "y": -171.099609375
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 730.000390625,
                            "y": 185.0
                        },
                        "control2": {
                            "x": 730.000390625,
                            "y": 480.0
                        },
                        "end": {
                            "x": 730.000390625,
                            "y": 480.0
                        },
                        "start": {
                            "x": 730.000390625,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 730.000390625,
                            "y": 657.0
                        },
                        "control2": {
                            "x": 587.000390625,
                            "y": 800.0
                        },
                        "end": {
                            "x": 410.000390625,
                            "y": 800.0
                        },
                        "start": {
                            "x": 730.000390625,
                            "y": 480.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 233.000390625,
                            "y": 800.0
                        },
                        "control2": {
                            "x": 90.000390625,
                            "y": 656.8974609375
                        },
                        "end": {
                            "x": 90.000390625,
                            "y": 480.0
                        },
                        "start": {
                            "x": 410.000390625,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 90.000390625,
                            "y": 0.0
                        },
                        "start": {
                            "x": 90.000390625,
                            "y": 480.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 40.0,
            "rightBearing": 42.0,
            "width": 822
        },
        "218": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 333.75,
                            "y": -64.0
                        },
                        "start": {
                            "x": 485.75,
                            "y": -171.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 730.000390625,
                            "y": 185.0
                        },
                        "control2": {
                            "x": 730.000390625,
                            "y": 480.0
                        },
                        "end": {
                            "x": 730.000390625,
                            "y": 480.0
                        },
                        "start": {
                            "x": 730.000390625,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 730.000390625,
                            "y": 657.0
                        },
                        "control2": {
                            "x": 587.000390625,
                            "y": 800.0
                        },
                        "end": {
                            "x": 410.000390625,
                            "y": 800.0
                        },
                        "start": {
                            "x": 730.000390625,
                            "y": 480.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 233.000390625,
                            "y": 800.0
                        },
                        "control2": {
                            "x": 90.000390625,
                            "y": 656.8974609375
                        },
                        "end": {
                            "x": 90.000390625,
                            "y": 480.0
                        },
                        "start": {
                            "x": 410.000390625,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 90.000390625,
                            "y": 0.0
                        },
                        "start": {
                            "x": 90.000390625,
                            "y": 480.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 40.0,
            "rightBearing": 42.0,
            "width": 822
        },
        "219": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 410.75,
                            "y": -171.099609375
                        },
                        "start": {
                            "x": 257.75,
                            "y": -64.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 563.75,
                            "y": -64.0
                        },
                        "start": {
                            "x": 410.75,
                            "y": -171.099609375
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 730.000390625,
                            "y": 185.0
                        },
                        "control2": {
                            "x": 730.000390625,
                            "y": 480.0
                        },
                        "end": {
                            "x": 730.000390625,
                            "y": 480.0
                        },
                        "start": {
                            "x": 730.000390625,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 730.000390625,
                            "y": 657.0
                        },
                        "control2": {
                            "x": 587.000390625,
                            "y": 800.0
                        },
                        "end": {
                            "x": 410.000390625,
                            "y": 800.0
                        },
                        "start": {
                            "x": 730.000390625,
                            "y": 480.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 233.000390625,
                            "y": 800.0
                        },
                        "control2": {
                            "x": 90.000390625,
                            "y": 656.8974609375
                        },
                        "end": {
                            "x": 90.000390625,
                            "y": 480.0
                        },
                        "start": {
                            "x": 410.000390625,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 90.000390625,
                            "y": 0.0
                        },
                        "start": {
                            "x": 90.000390625,
                            "y": 480.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 40.0,
            "rightBearing": 42.0,
            "width": 822
        },
        "220": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 486.0,
                            "y": -64.0
                        },
                        "start": {
                            "x": 486.0,
                            "y": -65.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 333.0,
                            "y": -64.0
                        },
                        "start": {
                            "x": 333.0,
                            "y": -65.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 730.0000390625,
                            "y": 185.0
                        },
                        "control2": {
                            "x": 730.0000390625,
                            "y": 480.0
                        },
                        "end": {
                            "x": 730.0000390625,
                            "y": 480.0
                        },
                        "start": {
                            "x": 730.0000390625,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 730.0000390625,
                            "y": 657.0
                        },
                        "control2": {
                            "x": 587.0000390625,
                            "y": 800.0
                        },
                        "end": {
                            "x": 410.0000390625,
                            "y": 800.0
                        },
                        "start": {
                            "x": 730.0000390625,
                            "y": 480.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 233.0000390625,
                            "y": 800.0
                        },
                        "control2": {
                            "x": 90.0000390625,
                            "y": 656.8974609375
                        },
                        "end": {
                            "x": 90.0000390625,
                            "y": 480.0
                        },
                        "start": {
                            "x": 410.0000390625,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 90.0000390625,
                            "y": 0.0
                        },
                        "start": {
                            "x": 90.0000390625,
                            "y": 480.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 40.0,
            "rightBearing": 40.0,
            "width": 820
        },
        "221": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 253.9,
                            "y": -64.0
                        },
                        "start": {
                            "x": 405.9,
                            "y": -171.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 319.0,
                            "y": 400.0
                        },
                        "start": {
                            "x": 319.0,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 319.0,
                            "y": 400.0
                        },
                        "start": {
                            "x": 30.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 609.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 319.0,
                            "y": 400.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": -20.0,
            "rightBearing": -20.0,
            "width": 639
        },
        "222": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 350.0,
                            "y": 120.0
                        },
                        "start": {
                            "x": 112.0,
                            "y": 120.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 483.0,
                            "y": 121.0
                        },
                        "control2": {
                            "x": 588.9996948242188,
                            "y": 229.0
                        },
                        "end": {
                            "x": 588.0,
                            "y": 362.0
                        },
                        "start": {
                            "x": 350.0,
                            "y": 120.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 587.0078125,
                            "y": 494.0
                        },
                        "control2": {
                            "x": 480.0,
                            "y": 600.0
                        },
                        "end": {
                            "x": 348.0,
                            "y": 600.0
                        },
                        "start": {
                            "x": 588.0,
                            "y": 362.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 110.0,
                            "y": 600.0
                        },
                        "start": {
                            "x": 348.0,
                            "y": 600.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 110.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": -0.00701341408796452,
            "width": 638
        },
        "223": {
            "glyph": [
                {
                    "points": {
                        "control1": {
                            "x": 280.0,
                            "y": 703.0
                        },
                        "control2": {
                            "x": 294.0,
                            "y": 724.0
                        },
                        "end": {
                            "x": 312.0,
                            "y": 742.0
                        },
                        "start": {
                            "x": 270.0,
                            "y": 679.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 348.0,
                            "y": 778.0
                        },
                        "control2": {
                            "x": 398.0,
                            "y": 800.0
                        },
                        "end": {
                            "x": 453.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 312.0,
                            "y": 742.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 562.0,
                            "y": 800.0
                        },
                        "control2": {
                            "x": 650.0,
                            "y": 711.0
                        },
                        "end": {
                            "x": 650.0,
                            "y": 602.0
                        },
                        "start": {
                            "x": 453.0,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 650.0,
                            "y": 548.0
                        },
                        "control2": {
                            "x": 629.7685546875,
                            "y": 490.943359375
                        },
                        "end": {
                            "x": 579.0,
                            "y": 454.0
                        },
                        "start": {
                            "x": 650.0,
                            "y": 602.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 355.0,
                            "y": 291.0
                        },
                        "start": {
                            "x": 579.0,
                            "y": 454.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 318.0,
                            "y": 264.0
                        },
                        "control2": {
                            "x": 288.0,
                            "y": 207.0
                        },
                        "end": {
                            "x": 288.0,
                            "y": 162.0
                        },
                        "start": {
                            "x": 355.0,
                            "y": 291.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 288.0,
                            "y": 73.0
                        },
                        "control2": {
                            "x": 360.0,
                            "y": 0.0
                        },
                        "end": {
                            "x": 450.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 288.0,
                            "y": 162.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 494.0,
                            "y": 0.0
                        },
                        "control2": {
                            "x": 535.0,
                            "y": 18.0
                        },
                        "end": {
                            "x": 564.0,
                            "y": 47.0
                        },
                        "start": {
                            "x": 450.0,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 579.0,
                            "y": 61.0
                        },
                        "control2": {
                            "x": 591.0,
                            "y": 78.0
                        },
                        "end": {
                            "x": 599.0,
                            "y": 97.0
                        },
                        "start": {
                            "x": 564.0,
                            "y": 47.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 80.0,
                            "y": 703.0
                        },
                        "control2": {
                            "x": 94.0,
                            "y": 724.0
                        },
                        "end": {
                            "x": 112.0,
                            "y": 742.0
                        },
                        "start": {
                            "x": 70.0,
                            "y": 679.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 148.0,
                            "y": 778.0
                        },
                        "control2": {
                            "x": 198.0,
                            "y": 800.0
                        },
                        "end": {
                            "x": 253.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 112.0,
                            "y": 742.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 362.0,
                            "y": 800.0
                        },
                        "control2": {
                            "x": 450.0,
                            "y": 711.0
                        },
                        "end": {
                            "x": 450.0,
                            "y": 602.0
                        },
                        "start": {
                            "x": 253.0,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 450.0,
                            "y": 548.0
                        },
                        "control2": {
                            "x": 429.7685546875,
                            "y": 490.943359375
                        },
                        "end": {
                            "x": 379.0,
                            "y": 454.0
                        },
                        "start": {
                            "x": 450.0,
                            "y": 602.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 155.0,
                            "y": 291.0
                        },
                        "start": {
                            "x": 379.0,
                            "y": 454.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 118.0,
                            "y": 264.0
                        },
                        "control2": {
                            "x": 88.0,
                            "y": 207.0
                        },
                        "end": {
                            "x": 88.0,
                            "y": 162.0
                        },
                        "start": {
                            "x": 155.0,
                            "y": 291.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 88.0,
                            "y": 73.0
                        },
                        "control2": {
                            "x": 160.0,
                            "y": 0.0
                        },
                        "end": {
                            "x": 250.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 88.0,
                            "y": 162.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 294.0,
                            "y": 0.0
                        },
                        "control2": {
                            "x": 335.0,
                            "y": 18.0
                        },
                        "end": {
                            "x": 364.0,
                            "y": 47.0
                        },
                        "start": {
                            "x": 250.0,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 379.0,
                            "y": 61.0
                        },
                        "control2": {
                            "x": 391.0,
                            "y": 78.0
                        },
                        "end": {
                            "x": 399.0,
                            "y": 97.0
                        },
                        "start": {
                            "x": 364.0,
                            "y": 47.0
                        }
                    },
                    "type": "C"
                }
            ],
            "leftBearing": 20.0,
            "rightBearing": 20.0,
            "width": 720
        },
        "224": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 381.0,
                            "y": -64.0
                        },
                        "start": {
                            "x": 228.0,
                            "y": -171.099609375
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 477.999804687,
                            "y": 488.0
                        },
                        "control2": {
                            "x": 307.999804687,
                            "y": 0.0
                        },
                        "end": {
                            "x": 307.999804687,
                            "y": 0.0
                        },
                        "start": {
                            "x": 586.999804687,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 29.999804687,
                            "y": 800.0
                        },
                        "start": {
                            "x": 307.999804687,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 491.999804687,
                            "y": 528.0
                        },
                        "start": {
                            "x": 124.999804687,
                            "y": 528.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": -20.0,
            "rightBearing": -20.0,
            "width": 617
        },
        "225": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 226.0,
                            "y": -64.0
                        },
                        "start": {
                            "x": 378.0,
                            "y": -171.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 477.999804687,
                            "y": 488.0
                        },
                        "control2": {
                            "x": 307.999804687,
                            "y": 0.0
                        },
                        "end": {
                            "x": 307.999804687,
                            "y": 0.0
                        },
                        "start": {
                            "x": 586.999804687,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 29.999804687,
                            "y": 800.0
                        },
                        "start": {
                            "x": 307.999804687,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 491.999804687,
                            "y": 528.0
                        },
                        "start": {
                            "x": 124.999804687,
                            "y": 528.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": -20.0,
            "rightBearing": -20.0,
            "width": 617
        },
        "226": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 304.9998,
                            "y": -171.099609375
                        },
                        "start": {
                            "x": 151.9998,
                            "y": -64.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 457.9998,
                            "y": -64.0
                        },
                        "start": {
                            "x": 304.9998,
                            "y": -171.099609375
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 477.999804687,
                            "y": 488.0
                        },
                        "control2": {
                            "x": 307.999804687,
                            "y": 0.0
                        },
                        "end": {
                            "x": 307.999804687,
                            "y": 0.0
                        },
                        "start": {
                            "x": 586.999804687,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 29.999804687,
                            "y": 800.0
                        },
                        "start": {
                            "x": 307.999804687,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 491.999804687,
                            "y": 528.0
                        },
                        "start": {
                            "x": 124.999804687,
                            "y": 528.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": -20.0,
            "rightBearing": -20.0,
            "width": 617
        },
        "227": {
            "glyph": [
                {
                    "points": {
                        "control1": {
                            "x": 417.103315625,
                            "y": -71.0498046875
                        },
                        "control2": {
                            "x": 394.829878125,
                            "y": -62.564453125
                        },
                        "end": {
                            "x": 372.5554640625,
                            "y": -62.564453125
                        },
                        "start": {
                            "x": 434.07401875,
                            "y": -88.0205078125
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 350.28105,
                            "y": -62.564453125
                        },
                        "control2": {
                            "x": 328.0076125,
                            "y": -71.0498046875
                        },
                        "end": {
                            "x": 311.036909375,
                            "y": -88.0205078125
                        },
                        "start": {
                            "x": 372.5554640625,
                            "y": -62.564453125
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 294.06620625,
                            "y": -104.9912109375
                        },
                        "control2": {
                            "x": 271.79276875,
                            "y": -113.4765625
                        },
                        "end": {
                            "x": 249.5183546875,
                            "y": -113.4765625
                        },
                        "start": {
                            "x": 311.036909375,
                            "y": -88.0205078125
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 227.243940625,
                            "y": -113.4765625
                        },
                        "control2": {
                            "x": 204.970503125,
                            "y": -104.9912109375
                        },
                        "end": {
                            "x": 187.9998,
                            "y": -88.0205078125
                        },
                        "start": {
                            "x": 249.5183546875,
                            "y": -113.4765625
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 477.999804687,
                            "y": 488.0
                        },
                        "control2": {
                            "x": 307.999804687,
                            "y": 0.0
                        },
                        "end": {
                            "x": 307.999804687,
                            "y": 0.0
                        },
                        "start": {
                            "x": 586.999804687,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 29.999804687,
                            "y": 800.0
                        },
                        "start": {
                            "x": 307.999804687,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 491.999804687,
                            "y": 528.0
                        },
                        "start": {
                            "x": 124.999804687,
                            "y": 528.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": -20.0,
            "rightBearing": -20.0,
            "width": 617
        },
        "228": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 380.0,
                            "y": -64.0
                        },
                        "start": {
                            "x": 380.0,
                            "y": -65.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 227.0,
                            "y": -64.0
                        },
                        "start": {
                            "x": 227.0,
                            "y": -65.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 477.999804687,
                            "y": 488.0
                        },
                        "control2": {
                            "x": 307.999804687,
                            "y": 0.0
                        },
                        "end": {
                            "x": 307.999804687,
                            "y": 0.0
                        },
                        "start": {
                            "x": 586.999804687,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 29.999804687,
                            "y": 800.0
                        },
                        "start": {
                            "x": 307.999804687,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 491.999804687,
                            "y": 528.0
                        },
                        "start": {
                            "x": 124.999804687,
                            "y": 528.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": -20.0,
            "rightBearing": -20.0,
            "width": 617
        },
        "229": {
            "glyph": [
                {
                    "points": {
                        "control1": {
                            "x": 180.0,
                            "y": -200.0
                        },
                        "control2": {
                            "x": 238.0,
                            "y": -258.0
                        },
                        "end": {
                            "x": 310.0,
                            "y": -258.0
                        },
                        "start": {
                            "x": 180.0,
                            "y": -128.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 382.0,
                            "y": -258.0
                        },
                        "control2": {
                            "x": 440.0,
                            "y": -200.0
                        },
                        "end": {
                            "x": 440.0,
                            "y": -128.0
                        },
                        "start": {
                            "x": 310.0,
                            "y": -258.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 440.0,
                            "y": -56.0
                        },
                        "control2": {
                            "x": 382.0,
                            "y": 2.0
                        },
                        "end": {
                            "x": 310.0,
                            "y": 2.0
                        },
                        "start": {
                            "x": 440.0,
                            "y": -128.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 238.0,
                            "y": 2.0
                        },
                        "control2": {
                            "x": 180.0,
                            "y": -56.0
                        },
                        "end": {
                            "x": 180.0,
                            "y": -128.0
                        },
                        "start": {
                            "x": 310.0,
                            "y": 2.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 477.999804687,
                            "y": 488.0
                        },
                        "control2": {
                            "x": 307.999804687,
                            "y": 0.0
                        },
                        "end": {
                            "x": 307.999804687,
                            "y": 0.0
                        },
                        "start": {
                            "x": 586.999804687,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 29.999804687,
                            "y": 800.0
                        },
                        "start": {
                            "x": 307.999804687,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 491.999804687,
                            "y": 528.0
                        },
                        "start": {
                            "x": 124.999804687,
                            "y": 528.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": -20.0,
            "rightBearing": -20.0,
            "width": 617
        },
        "230": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 867.0,
                            "y": 400.0
                        },
                        "start": {
                            "x": 507.0,
                            "y": 400.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 867.0,
                            "y": 400.0
                        },
                        "start": {
                            "x": 507.0,
                            "y": 400.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 507.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 947.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 507.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 507.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 947.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 507.0,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 41.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 507.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 507.0,
                            "y": 528.0
                        },
                        "start": {
                            "x": 201.0,
                            "y": 526.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": -9.0,
            "rightBearing": 55.0,
            "width": 1052
        },
        "231": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 411.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 411.0,
                            "y": 951.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 579.0,
                            "y": 764.0
                        },
                        "control2": {
                            "x": 500.0,
                            "y": 800.0
                        },
                        "end": {
                            "x": 411.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 637.0,
                            "y": 706.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 234.0,
                            "y": 800.0
                        },
                        "control2": {
                            "x": 90.0,
                            "y": 657.0
                        },
                        "end": {
                            "x": 90.0,
                            "y": 480.0
                        },
                        "start": {
                            "x": 411.0,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 90.0,
                            "y": 320.0
                        },
                        "start": {
                            "x": 90.0,
                            "y": 480.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 90.0,
                            "y": 143.0
                        },
                        "control2": {
                            "x": 234.0,
                            "y": 0.0
                        },
                        "end": {
                            "x": 411.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 90.0,
                            "y": 320.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 500.0,
                            "y": 0.0
                        },
                        "control2": {
                            "x": 579.0,
                            "y": 36.0
                        },
                        "end": {
                            "x": 637.0,
                            "y": 94.0
                        },
                        "start": {
                            "x": 411.0,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                }
            ],
            "leftBearing": 40.0,
            "rightBearing": 20.0,
            "width": 707
        },
        "232": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 404.75,
                            "y": -64.0
                        },
                        "start": {
                            "x": 251.75,
                            "y": -171.099609375
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 470.000390625,
                            "y": 400.0
                        },
                        "start": {
                            "x": 110.000390625,
                            "y": 400.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 470.000390625,
                            "y": 400.0
                        },
                        "start": {
                            "x": 110.000390625,
                            "y": 400.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 110.000390625,
                            "y": 0.0
                        },
                        "start": {
                            "x": 550.000390625,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 110.000390625,
                            "y": 800.0
                        },
                        "start": {
                            "x": 110.000390625,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 550.000390625,
                            "y": 800.0
                        },
                        "start": {
                            "x": 110.000390625,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 10.0,
            "width": 610
        },
        "233": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 262.75,
                            "y": -64.0
                        },
                        "start": {
                            "x": 414.75,
                            "y": -171.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 470.000390625,
                            "y": 400.0
                        },
                        "start": {
                            "x": 110.000390625,
                            "y": 400.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 470.000390625,
                            "y": 400.0
                        },
                        "start": {
                            "x": 110.000390625,
                            "y": 400.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 110.000390625,
                            "y": 0.0
                        },
                        "start": {
                            "x": 550.000390625,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 110.000390625,
                            "y": 800.0
                        },
                        "start": {
                            "x": 110.000390625,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 550.000390625,
                            "y": 800.0
                        },
                        "start": {
                            "x": 110.000390625,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 10.0,
            "width": 610
        },
        "234": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 335.7504,
                            "y": -171.099609375
                        },
                        "start": {
                            "x": 182.7504,
                            "y": -64.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 488.7504,
                            "y": -64.0
                        },
                        "start": {
                            "x": 335.7504,
                            "y": -171.099609375
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 470.000390625,
                            "y": 400.0
                        },
                        "start": {
                            "x": 110.000390625,
                            "y": 400.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 470.000390625,
                            "y": 400.0
                        },
                        "start": {
                            "x": 110.000390625,
                            "y": 400.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 110.000390625,
                            "y": 0.0
                        },
                        "start": {
                            "x": 550.000390625,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 110.000390625,
                            "y": 800.0
                        },
                        "start": {
                            "x": 110.000390625,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 550.000390625,
                            "y": 800.0
                        },
                        "start": {
                            "x": 110.000390625,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 10.0,
            "width": 610
        },
        "235": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 405.75,
                            "y": -61.99996999999996
                        },
                        "start": {
                            "x": 405.75,
                            "y": -62.99996999999996
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 252.75,
                            "y": -61.99996999999996
                        },
                        "start": {
                            "x": 252.75,
                            "y": -62.99996999999996
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 469.999804688,
                            "y": 400.0
                        },
                        "start": {
                            "x": 109.999804688,
                            "y": 400.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 469.999804688,
                            "y": 400.0
                        },
                        "start": {
                            "x": 109.999804688,
                            "y": 400.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 109.999804688,
                            "y": 0.0
                        },
                        "start": {
                            "x": 549.999804688,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 109.999804688,
                            "y": 800.0
                        },
                        "start": {
                            "x": 109.999804688,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 549.999804688,
                            "y": 800.0
                        },
                        "start": {
                            "x": 109.999804688,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 10.0,
            "width": 610
        },
        "236": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 198.4998,
                            "y": -64.0
                        },
                        "start": {
                            "x": 45.49979999999999,
                            "y": -171.099609375
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 123.4998,
                            "y": 800.0
                        },
                        "start": {
                            "x": 123.4998,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": -4.5,
            "rightBearing": -4.5,
            "width": 244
        },
        "237": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 46.0004,
                            "y": -64.0
                        },
                        "start": {
                            "x": 198.0004,
                            "y": -171.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 126.00039,
                            "y": 800.0
                        },
                        "start": {
                            "x": 126.00039,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": -4.0,
            "rightBearing": -4.0,
            "width": 244
        },
        "238": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 122.0,
                            "y": -171.099609375
                        },
                        "start": {
                            "x": -31.0,
                            "y": -64.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 275.0,
                            "y": -64.0
                        },
                        "start": {
                            "x": 122.0,
                            "y": -171.099609375
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 118.99971,
                            "y": 800.0
                        },
                        "start": {
                            "x": 118.99971,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": -81.0,
            "rightBearing": -81.0,
            "width": 244
        },
        "239": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 198.0002,
                            "y": -65.99996999999996
                        },
                        "start": {
                            "x": 198.0002,
                            "y": -66.99996999999996
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 45.00020000000001,
                            "y": -65.99996999999996
                        },
                        "start": {
                            "x": 45.00020000000001,
                            "y": -66.99996999999996
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 133.0002,
                            "y": 800.0
                        },
                        "start": {
                            "x": 133.0002,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": -5.0,
            "rightBearing": -4.0,
            "width": 244
        },
        "240": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 262.5,
                            "y": 400.0
                        },
                        "start": {
                            "x": -0.5,
                            "y": 400.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 132.5,
                            "y": 0.0
                        },
                        "start": {
                            "x": 132.5,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 332.5,
                            "y": 0.0
                        },
                        "start": {
                            "x": 132.5,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 434.5,
                            "y": 0.0
                        },
                        "control2": {
                            "x": 536.5,
                            "y": 39.0
                        },
                        "end": {
                            "x": 614.5,
                            "y": 117.0
                        },
                        "start": {
                            "x": 332.5,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 770.5,
                            "y": 273.0
                        },
                        "control2": {
                            "x": 770.5,
                            "y": 527.0
                        },
                        "end": {
                            "x": 614.5,
                            "y": 683.0
                        },
                        "start": {
                            "x": 614.5,
                            "y": 117.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 536.5,
                            "y": 761.0
                        },
                        "control2": {
                            "x": 432.5,
                            "y": 800.0
                        },
                        "end": {
                            "x": 331.5,
                            "y": 800.0
                        },
                        "start": {
                            "x": 614.5,
                            "y": 683.0
                        }
                    },
                    "type": "C"
                }
            ],
            "leftBearing": -50.5,
            "rightBearing": 19.5,
            "width": 801
        },
        "241": {
            "glyph": [
                {
                    "points": {
                        "control1": {
                            "x": 495.853515625,
                            "y": -71.0498046875
                        },
                        "control2": {
                            "x": 473.580078125,
                            "y": -62.564453125
                        },
                        "end": {
                            "x": 451.3056640625,
                            "y": -62.564453125
                        },
                        "start": {
                            "x": 512.82421875,
                            "y": -88.0205078125
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 429.03125,
                            "y": -62.564453125
                        },
                        "control2": {
                            "x": 406.7578125,
                            "y": -71.0498046875
                        },
                        "end": {
                            "x": 389.787109375,
                            "y": -88.0205078125
                        },
                        "start": {
                            "x": 451.3056640625,
                            "y": -62.564453125
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 372.81640625,
                            "y": -104.9912109375
                        },
                        "control2": {
                            "x": 350.54296875,
                            "y": -113.4765625
                        },
                        "end": {
                            "x": 328.2685546875,
                            "y": -113.4765625
                        },
                        "start": {
                            "x": 389.787109375,
                            "y": -88.0205078125
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 305.994140625,
                            "y": -113.4765625
                        },
                        "control2": {
                            "x": 283.720703125,
                            "y": -104.9912109375
                        },
                        "end": {
                            "x": 266.75,
                            "y": -88.0205078125
                        },
                        "start": {
                            "x": 328.2685546875,
                            "y": -113.4765625
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 670.000390625,
                            "y": 800.0
                        },
                        "start": {
                            "x": 110.000390625,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 110.000390625,
                            "y": 0.0
                        },
                        "start": {
                            "x": 110.000390625,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 670.000390625,
                            "y": 800.0
                        },
                        "start": {
                            "x": 670.000390625,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 60.0,
            "width": 780
        },
        "242": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 545.0,
                            "y": -64.0
                        },
                        "start": {
                            "x": 392.0,
                            "y": -171.099609375
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 248.9999023437,
                            "y": 800.0
                        },
                        "control2": {
                            "x": 69.9999023437,
                            "y": 621.0
                        },
                        "end": {
                            "x": 69.9999023437,
                            "y": 400.0
                        },
                        "start": {
                            "x": 469.9999023437,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 69.9999023437,
                            "y": 179.0
                        },
                        "control2": {
                            "x": 248.9999023437,
                            "y": 0.0
                        },
                        "end": {
                            "x": 469.9999023437,
                            "y": 0.0
                        },
                        "start": {
                            "x": 69.9999023437,
                            "y": 400.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 690.9999023437,
                            "y": 0.0
                        },
                        "control2": {
                            "x": 869.9999023437,
                            "y": 179.0
                        },
                        "end": {
                            "x": 869.9999023437,
                            "y": 400.0
                        },
                        "start": {
                            "x": 469.9999023437,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 869.9999023437,
                            "y": 621.0
                        },
                        "control2": {
                            "x": 690.9999023437,
                            "y": 800.0
                        },
                        "end": {
                            "x": 469.9999023437,
                            "y": 800.0
                        },
                        "start": {
                            "x": 869.9999023437,
                            "y": 400.0
                        }
                    },
                    "type": "C"
                }
            ],
            "leftBearing": 20.0,
            "rightBearing": 20.0,
            "width": 940
        },
        "243": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 390.0,
                            "y": -64.0
                        },
                        "start": {
                            "x": 542.0,
                            "y": -171.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 248.9999023437,
                            "y": 800.0
                        },
                        "control2": {
                            "x": 69.9999023437,
                            "y": 621.0
                        },
                        "end": {
                            "x": 69.9999023437,
                            "y": 400.0
                        },
                        "start": {
                            "x": 469.9999023437,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 69.9999023437,
                            "y": 179.0
                        },
                        "control2": {
                            "x": 248.9999023437,
                            "y": 0.0
                        },
                        "end": {
                            "x": 469.9999023437,
                            "y": 0.0
                        },
                        "start": {
                            "x": 69.9999023437,
                            "y": 400.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 690.9999023437,
                            "y": 0.0
                        },
                        "control2": {
                            "x": 869.9999023437,
                            "y": 179.0
                        },
                        "end": {
                            "x": 869.9999023437,
                            "y": 400.0
                        },
                        "start": {
                            "x": 469.9999023437,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 869.9999023437,
                            "y": 621.0
                        },
                        "control2": {
                            "x": 690.9999023437,
                            "y": 800.0
                        },
                        "end": {
                            "x": 469.9999023437,
                            "y": 800.0
                        },
                        "start": {
                            "x": 869.9999023437,
                            "y": 400.0
                        }
                    },
                    "type": "C"
                }
            ],
            "leftBearing": 20.0,
            "rightBearing": 20.0,
            "width": 940
        },
        "244": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 471.0,
                            "y": -171.099609375
                        },
                        "start": {
                            "x": 318.0,
                            "y": -64.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 624.0,
                            "y": -64.0
                        },
                        "start": {
                            "x": 471.0,
                            "y": -171.099609375
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 248.9999023437,
                            "y": 800.0
                        },
                        "control2": {
                            "x": 69.9999023437,
                            "y": 621.0
                        },
                        "end": {
                            "x": 69.9999023437,
                            "y": 400.0
                        },
                        "start": {
                            "x": 469.9999023437,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 69.9999023437,
                            "y": 179.0
                        },
                        "control2": {
                            "x": 248.9999023437,
                            "y": 0.0
                        },
                        "end": {
                            "x": 469.9999023437,
                            "y": 0.0
                        },
                        "start": {
                            "x": 69.9999023437,
                            "y": 400.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 690.9999023437,
                            "y": 0.0
                        },
                        "control2": {
                            "x": 869.9999023437,
                            "y": 179.0
                        },
                        "end": {
                            "x": 869.9999023437,
                            "y": 400.0
                        },
                        "start": {
                            "x": 469.9999023437,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 869.9999023437,
                            "y": 621.0
                        },
                        "control2": {
                            "x": 690.9999023437,
                            "y": 800.0
                        },
                        "end": {
                            "x": 469.9999023437,
                            "y": 800.0
                        },
                        "start": {
                            "x": 869.9999023437,
                            "y": 400.0
                        }
                    },
                    "type": "C"
                }
            ],
            "leftBearing": 20.0,
            "rightBearing": 20.0,
            "width": 940
        },
        "245": {
            "glyph": [
                {
                    "points": {
                        "control1": {
                            "x": 579.103515625,
                            "y": -71.0498046875
                        },
                        "control2": {
                            "x": 556.830078125,
                            "y": -62.564453125
                        },
                        "end": {
                            "x": 534.5556640625,
                            "y": -62.564453125
                        },
                        "start": {
                            "x": 596.07421875,
                            "y": -88.0205078125
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 512.28125,
                            "y": -62.564453125
                        },
                        "control2": {
                            "x": 490.0078125,
                            "y": -71.0498046875
                        },
                        "end": {
                            "x": 473.037109375,
                            "y": -88.0205078125
                        },
                        "start": {
                            "x": 534.5556640625,
                            "y": -62.564453125
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 456.06640625,
                            "y": -104.9912109375
                        },
                        "control2": {
                            "x": 433.79296875,
                            "y": -113.4765625
                        },
                        "end": {
                            "x": 411.5185546875,
                            "y": -113.4765625
                        },
                        "start": {
                            "x": 473.037109375,
                            "y": -88.0205078125
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 389.244140625,
                            "y": -113.4765625
                        },
                        "control2": {
                            "x": 366.970703125,
                            "y": -104.9912109375
                        },
                        "end": {
                            "x": 350.0,
                            "y": -88.0205078125
                        },
                        "start": {
                            "x": 411.5185546875,
                            "y": -113.4765625
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 248.9999023437,
                            "y": 800.0
                        },
                        "control2": {
                            "x": 69.9999023437,
                            "y": 621.0
                        },
                        "end": {
                            "x": 69.9999023437,
                            "y": 400.0
                        },
                        "start": {
                            "x": 469.9999023437,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 69.9999023437,
                            "y": 179.0
                        },
                        "control2": {
                            "x": 248.9999023437,
                            "y": 0.0
                        },
                        "end": {
                            "x": 469.9999023437,
                            "y": 0.0
                        },
                        "start": {
                            "x": 69.9999023437,
                            "y": 400.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 690.9999023437,
                            "y": 0.0
                        },
                        "control2": {
                            "x": 869.9999023437,
                            "y": 179.0
                        },
                        "end": {
                            "x": 869.9999023437,
                            "y": 400.0
                        },
                        "start": {
                            "x": 469.9999023437,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 869.9999023437,
                            "y": 621.0
                        },
                        "control2": {
                            "x": 690.9999023437,
                            "y": 800.0
                        },
                        "end": {
                            "x": 469.9999023437,
                            "y": 800.0
                        },
                        "start": {
                            "x": 869.9999023437,
                            "y": 400.0
                        }
                    },
                    "type": "C"
                }
            ],
            "leftBearing": 20.0,
            "rightBearing": 20.0,
            "width": 940
        },
        "246": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 543.0,
                            "y": -64.0
                        },
                        "start": {
                            "x": 543.0,
                            "y": -65.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 390.0,
                            "y": -64.0
                        },
                        "start": {
                            "x": 390.0,
                            "y": -65.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 248.9999023437,
                            "y": 800.0
                        },
                        "control2": {
                            "x": 69.9999023437,
                            "y": 621.0
                        },
                        "end": {
                            "x": 69.9999023437,
                            "y": 400.0
                        },
                        "start": {
                            "x": 469.9999023437,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 69.9999023437,
                            "y": 179.0
                        },
                        "control2": {
                            "x": 248.9999023437,
                            "y": 0.0
                        },
                        "end": {
                            "x": 469.9999023437,
                            "y": 0.0
                        },
                        "start": {
                            "x": 69.9999023437,
                            "y": 400.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 690.9999023437,
                            "y": 0.0
                        },
                        "control2": {
                            "x": 869.9999023437,
                            "y": 179.0
                        },
                        "end": {
                            "x": 869.9999023437,
                            "y": 400.0
                        },
                        "start": {
                            "x": 469.9999023437,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 869.9999023437,
                            "y": 621.0
                        },
                        "control2": {
                            "x": 690.9999023437,
                            "y": 800.0
                        },
                        "end": {
                            "x": 469.9999023437,
                            "y": 800.0
                        },
                        "start": {
                            "x": 869.9999023437,
                            "y": 400.0
                        }
                    },
                    "type": "C"
                }
            ],
            "leftBearing": 20.0,
            "rightBearing": 20.0,
            "width": 940
        },
        "247": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 110.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 365.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 60.0,
            "width": 475
        },
        "248": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 350.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 605.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 249.0,
                            "y": 800.0
                        },
                        "control2": {
                            "x": 70.0,
                            "y": 621.0
                        },
                        "end": {
                            "x": 70.0,
                            "y": 400.0
                        },
                        "start": {
                            "x": 470.0,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 70.0,
                            "y": 179.0
                        },
                        "control2": {
                            "x": 249.0,
                            "y": 0.0
                        },
                        "end": {
                            "x": 470.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 70.0,
                            "y": 400.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 691.0,
                            "y": 0.0
                        },
                        "control2": {
                            "x": 870.0,
                            "y": 179.0
                        },
                        "end": {
                            "x": 870.0,
                            "y": 400.0
                        },
                        "start": {
                            "x": 470.0,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 870.0,
                            "y": 621.0
                        },
                        "control2": {
                            "x": 691.0,
                            "y": 800.0
                        },
                        "end": {
                            "x": 470.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 870.0,
                            "y": 400.0
                        }
                    },
                    "type": "C"
                }
            ],
            "leftBearing": 20.0,
            "rightBearing": 20.0,
            "width": 940
        },
        "249": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 488.75,
                            "y": -64.0
                        },
                        "start": {
                            "x": 335.75,
                            "y": -171.099609375
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 730.000390625,
                            "y": 185.0
                        },
                        "control2": {
                            "x": 730.000390625,
                            "y": 480.0
                        },
                        "end": {
                            "x": 730.000390625,
                            "y": 480.0
                        },
                        "start": {
                            "x": 730.000390625,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 730.000390625,
                            "y": 657.0
                        },
                        "control2": {
                            "x": 587.000390625,
                            "y": 800.0
                        },
                        "end": {
                            "x": 410.000390625,
                            "y": 800.0
                        },
                        "start": {
                            "x": 730.000390625,
                            "y": 480.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 233.000390625,
                            "y": 800.0
                        },
                        "control2": {
                            "x": 90.000390625,
                            "y": 656.8974609375
                        },
                        "end": {
                            "x": 90.000390625,
                            "y": 480.0
                        },
                        "start": {
                            "x": 410.000390625,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 90.000390625,
                            "y": 0.0
                        },
                        "start": {
                            "x": 90.000390625,
                            "y": 480.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 40.0,
            "rightBearing": 42.0,
            "width": 822
        },
        "250": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 333.75,
                            "y": -64.0
                        },
                        "start": {
                            "x": 485.75,
                            "y": -171.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 730.000390625,
                            "y": 185.0
                        },
                        "control2": {
                            "x": 730.000390625,
                            "y": 480.0
                        },
                        "end": {
                            "x": 730.000390625,
                            "y": 480.0
                        },
                        "start": {
                            "x": 730.000390625,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 730.000390625,
                            "y": 657.0
                        },
                        "control2": {
                            "x": 587.000390625,
                            "y": 800.0
                        },
                        "end": {
                            "x": 410.000390625,
                            "y": 800.0
                        },
                        "start": {
                            "x": 730.000390625,
                            "y": 480.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 233.000390625,
                            "y": 800.0
                        },
                        "control2": {
                            "x": 90.000390625,
                            "y": 656.8974609375
                        },
                        "end": {
                            "x": 90.000390625,
                            "y": 480.0
                        },
                        "start": {
                            "x": 410.000390625,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 90.000390625,
                            "y": 0.0
                        },
                        "start": {
                            "x": 90.000390625,
                            "y": 480.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 40.0,
            "rightBearing": 42.0,
            "width": 822
        },
        "251": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 410.75,
                            "y": -171.099609375
                        },
                        "start": {
                            "x": 257.75,
                            "y": -64.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 563.75,
                            "y": -64.0
                        },
                        "start": {
                            "x": 410.75,
                            "y": -171.099609375
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 730.000390625,
                            "y": 185.0
                        },
                        "control2": {
                            "x": 730.000390625,
                            "y": 480.0
                        },
                        "end": {
                            "x": 730.000390625,
                            "y": 480.0
                        },
                        "start": {
                            "x": 730.000390625,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 730.000390625,
                            "y": 657.0
                        },
                        "control2": {
                            "x": 587.000390625,
                            "y": 800.0
                        },
                        "end": {
                            "x": 410.000390625,
                            "y": 800.0
                        },
                        "start": {
                            "x": 730.000390625,
                            "y": 480.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 233.000390625,
                            "y": 800.0
                        },
                        "control2": {
                            "x": 90.000390625,
                            "y": 656.8974609375
                        },
                        "end": {
                            "x": 90.000390625,
                            "y": 480.0
                        },
                        "start": {
                            "x": 410.000390625,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 90.000390625,
                            "y": 0.0
                        },
                        "start": {
                            "x": 90.000390625,
                            "y": 480.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 40.0,
            "rightBearing": 42.0,
            "width": 822
        },
        "252": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 486.0,
                            "y": -64.0
                        },
                        "start": {
                            "x": 486.0,
                            "y": -65.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 333.0,
                            "y": -64.0
                        },
                        "start": {
                            "x": 333.0,
                            "y": -65.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 730.0000390625,
                            "y": 185.0
                        },
                        "control2": {
                            "x": 730.0000390625,
                            "y": 480.0
                        },
                        "end": {
                            "x": 730.0000390625,
                            "y": 480.0
                        },
                        "start": {
                            "x": 730.0000390625,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 730.0000390625,
                            "y": 657.0
                        },
                        "control2": {
                            "x": 587.0000390625,
                            "y": 800.0
                        },
                        "end": {
                            "x": 410.0000390625,
                            "y": 800.0
                        },
                        "start": {
                            "x": 730.0000390625,
                            "y": 480.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 233.0000390625,
                            "y": 800.0
                        },
                        "control2": {
                            "x": 90.0000390625,
                            "y": 656.8974609375
                        },
                        "end": {
                            "x": 90.0000390625,
                            "y": 480.0
                        },
                        "start": {
                            "x": 410.0000390625,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 90.0000390625,
                            "y": 0.0
                        },
                        "start": {
                            "x": 90.0000390625,
                            "y": 480.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 40.0,
            "rightBearing": 40.0,
            "width": 820
        },
        "253": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 253.9,
                            "y": -64.0
                        },
                        "start": {
                            "x": 405.9,
                            "y": -171.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 319.0,
                            "y": 400.0
                        },
                        "start": {
                            "x": 319.0,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 319.0,
                            "y": 400.0
                        },
                        "start": {
                            "x": 30.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 609.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 319.0,
                            "y": 400.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": -20.0,
            "rightBearing": -20.0,
            "width": 639
        },
        "254": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 350.0,
                            "y": 120.0
                        },
                        "start": {
                            "x": 112.0,
                            "y": 120.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 483.0,
                            "y": 121.0
                        },
                        "control2": {
                            "x": 588.9996948242188,
                            "y": 229.0
                        },
                        "end": {
                            "x": 588.0,
                            "y": 362.0
                        },
                        "start": {
                            "x": 350.0,
                            "y": 120.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 587.0078125,
                            "y": 494.0
                        },
                        "control2": {
                            "x": 480.0,
                            "y": 600.0
                        },
                        "end": {
                            "x": 348.0,
                            "y": 600.0
                        },
                        "start": {
                            "x": 588.0,
                            "y": 362.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 110.0,
                            "y": 600.0
                        },
                        "start": {
                            "x": 348.0,
                            "y": 600.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 110.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": -0.007017530237817482,
            "width": 638
        },
        "255": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 393.0,
                            "y": -64.0
                        },
                        "start": {
                            "x": 393.0,
                            "y": -65.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 240.0,
                            "y": -64.0
                        },
                        "start": {
                            "x": 240.0,
                            "y": -65.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 319.000234375,
                            "y": 400.0
                        },
                        "start": {
                            "x": 319.000234375,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 319.000234375,
                            "y": 400.0
                        },
                        "start": {
                            "x": 30.000234375,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 609.000234375,
                            "y": 0.0
                        },
                        "start": {
                            "x": 319.000234375,
                            "y": 400.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": -20.0,
            "rightBearing": -20.0,
            "width": 639
        },
        "33": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 110.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 612.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 60.0,
            "width": 220
        },
        "34": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 274.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 274.0,
                            "y": 151.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 110.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 151.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 60.0,
            "width": 384
        },
        "35": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 684.0,
                            "y": 315.0
                        },
                        "start": {
                            "x": 187.0,
                            "y": 315.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 617.0,
                            "y": 537.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 537.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 422.5,
                            "y": 800.0
                        },
                        "start": {
                            "x": 677.5,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 142.5,
                            "y": 800.0
                        },
                        "start": {
                            "x": 397.5,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 60.0,
            "width": 794
        },
        "36": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 293.0,
                            "y": 871.0
                        },
                        "start": {
                            "x": 293.0,
                            "y": 801.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 290.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 290.0,
                            "y": -70.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 120.0,
                            "y": 704.0
                        },
                        "control2": {
                            "x": 134.0,
                            "y": 725.0
                        },
                        "end": {
                            "x": 152.0,
                            "y": 743.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 680.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 188.0,
                            "y": 779.0
                        },
                        "control2": {
                            "x": 238.0,
                            "y": 801.0
                        },
                        "end": {
                            "x": 293.0,
                            "y": 801.0
                        },
                        "start": {
                            "x": 152.0,
                            "y": 743.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 402.0,
                            "y": 801.0
                        },
                        "control2": {
                            "x": 490.0,
                            "y": 712.0
                        },
                        "end": {
                            "x": 490.0,
                            "y": 603.0
                        },
                        "start": {
                            "x": 293.0,
                            "y": 801.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 490.0,
                            "y": 549.0
                        },
                        "control2": {
                            "x": 469.0,
                            "y": 492.0
                        },
                        "end": {
                            "x": 419.0,
                            "y": 454.0
                        },
                        "start": {
                            "x": 490.0,
                            "y": 603.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 195.0,
                            "y": 291.0
                        },
                        "start": {
                            "x": 419.0,
                            "y": 454.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 158.0,
                            "y": 264.0
                        },
                        "control2": {
                            "x": 128.0,
                            "y": 207.0
                        },
                        "end": {
                            "x": 128.0,
                            "y": 162.0
                        },
                        "start": {
                            "x": 195.0,
                            "y": 291.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 128.0,
                            "y": 73.0
                        },
                        "control2": {
                            "x": 200.0,
                            "y": 0.0
                        },
                        "end": {
                            "x": 290.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 128.0,
                            "y": 162.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 334.0,
                            "y": 0.0
                        },
                        "control2": {
                            "x": 375.0,
                            "y": 18.0
                        },
                        "end": {
                            "x": 404.0,
                            "y": 47.0
                        },
                        "start": {
                            "x": 290.0,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 419.0,
                            "y": 61.0
                        },
                        "control2": {
                            "x": 431.0,
                            "y": 78.0
                        },
                        "end": {
                            "x": 439.0,
                            "y": 97.0
                        },
                        "start": {
                            "x": 404.0,
                            "y": 47.0
                        }
                    },
                    "type": "C"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 60.0,
            "width": 600
        },
        "37": {
            "glyph": [
                {
                    "points": {
                        "control1": {
                            "x": 370.0,
                            "y": 598.0
                        },
                        "control2": {
                            "x": 428.0,
                            "y": 540.0
                        },
                        "end": {
                            "x": 500.0,
                            "y": 540.0
                        },
                        "start": {
                            "x": 370.0,
                            "y": 670.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 572.0,
                            "y": 540.0
                        },
                        "control2": {
                            "x": 630.0,
                            "y": 598.0
                        },
                        "end": {
                            "x": 630.0,
                            "y": 670.0
                        },
                        "start": {
                            "x": 500.0,
                            "y": 540.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 630.0,
                            "y": 742.0
                        },
                        "control2": {
                            "x": 572.0,
                            "y": 800.0
                        },
                        "end": {
                            "x": 500.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 630.0,
                            "y": 670.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 428.0,
                            "y": 800.0
                        },
                        "control2": {
                            "x": 370.0,
                            "y": 742.0
                        },
                        "end": {
                            "x": 370.0,
                            "y": 670.0
                        },
                        "start": {
                            "x": 500.0,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 110.0,
                            "y": 58.0
                        },
                        "control2": {
                            "x": 168.0,
                            "y": 0.0
                        },
                        "end": {
                            "x": 240.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 130.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 312.0,
                            "y": 0.0
                        },
                        "control2": {
                            "x": 370.0,
                            "y": 58.0
                        },
                        "end": {
                            "x": 370.0,
                            "y": 130.0
                        },
                        "start": {
                            "x": 240.0,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 370.0,
                            "y": 202.0
                        },
                        "control2": {
                            "x": 312.0,
                            "y": 260.0
                        },
                        "end": {
                            "x": 240.0,
                            "y": 260.0
                        },
                        "start": {
                            "x": 370.0,
                            "y": 130.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 168.0,
                            "y": 260.0
                        },
                        "control2": {
                            "x": 110.0,
                            "y": 202.0
                        },
                        "end": {
                            "x": 110.0,
                            "y": 130.0
                        },
                        "start": {
                            "x": 240.0,
                            "y": 260.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 110.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 630.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 60.0,
            "width": 740
        },
        "38": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 245.0,
                            "y": 291.0
                        },
                        "start": {
                            "x": 723.0,
                            "y": 733.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 211.482421875,
                            "y": 260.005859375
                        },
                        "control2": {
                            "x": 178.0,
                            "y": 207.0
                        },
                        "end": {
                            "x": 178.0,
                            "y": 162.0
                        },
                        "start": {
                            "x": 245.0,
                            "y": 291.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 178.0,
                            "y": 73.0
                        },
                        "control2": {
                            "x": 250.0,
                            "y": 0.0
                        },
                        "end": {
                            "x": 340.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 178.0,
                            "y": 162.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 430.0,
                            "y": 0.0
                        },
                        "control2": {
                            "x": 502.0,
                            "y": 73.0
                        },
                        "end": {
                            "x": 502.0,
                            "y": 162.0
                        },
                        "start": {
                            "x": 340.0,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 502.0,
                            "y": 207.0
                        },
                        "control2": {
                            "x": 472.0,
                            "y": 264.0
                        },
                        "end": {
                            "x": 435.0,
                            "y": 291.0
                        },
                        "start": {
                            "x": 502.0,
                            "y": 162.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 211.0,
                            "y": 454.0
                        },
                        "start": {
                            "x": 435.0,
                            "y": 291.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 161.0,
                            "y": 492.0
                        },
                        "control2": {
                            "x": 140.0,
                            "y": 549.0
                        },
                        "end": {
                            "x": 140.0,
                            "y": 603.0
                        },
                        "start": {
                            "x": 211.0,
                            "y": 454.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 140.0,
                            "y": 712.0
                        },
                        "control2": {
                            "x": 228.0,
                            "y": 801.0
                        },
                        "end": {
                            "x": 337.0,
                            "y": 801.0
                        },
                        "start": {
                            "x": 140.0,
                            "y": 603.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 392.0,
                            "y": 801.0
                        },
                        "control2": {
                            "x": 431.0,
                            "y": 788.423828125
                        },
                        "end": {
                            "x": 467.0,
                            "y": 753.0
                        },
                        "start": {
                            "x": 337.0,
                            "y": 801.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 491.3740234375,
                            "y": 744.0
                        },
                        "control2": {
                            "x": 717.0,
                            "y": 507.0
                        },
                        "end": {
                            "x": 717.0,
                            "y": 507.0
                        },
                        "start": {
                            "x": 467.0,
                            "y": 753.0
                        }
                    },
                    "type": "C"
                }
            ],
            "leftBearing": 90.0,
            "rightBearing": 30.0,
            "width": 803
        },
        "39": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 110.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 151.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 60.0,
            "width": 220
        },
        "40": {
            "glyph": [
                {
                    "points": {
                        "control1": {
                            "x": 136.0,
                            "y": 124.0
                        },
                        "control2": {
                            "x": 110.0,
                            "y": 259.0
                        },
                        "end": {
                            "x": 110.0,
                            "y": 399.0
                        },
                        "start": {
                            "x": 183.0,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 110.0,
                            "y": 539.0
                        },
                        "control2": {
                            "x": 135.0,
                            "y": 677.0
                        },
                        "end": {
                            "x": 182.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 399.0
                        }
                    },
                    "type": "C"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 60.0,
            "width": 293
        },
        "41": {
            "glyph": [
                {
                    "points": {
                        "control1": {
                            "x": 157.0,
                            "y": 124.0
                        },
                        "control2": {
                            "x": 183.0,
                            "y": 260.0
                        },
                        "end": {
                            "x": 183.0,
                            "y": 400.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 183.0,
                            "y": 540.0
                        },
                        "control2": {
                            "x": 157.0,
                            "y": 677.0
                        },
                        "end": {
                            "x": 110.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 183.0,
                            "y": 400.0
                        }
                    },
                    "type": "C"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 60.0,
            "width": 293
        },
        "42": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 110.0,
                            "y": 197.25
                        },
                        "start": {
                            "x": 337.765625,
                            "y": 65.75
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 110.0,
                            "y": 197.25
                        },
                        "start": {
                            "x": 337.765625,
                            "y": 65.75
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 110.0,
                            "y": 65.75
                        },
                        "start": {
                            "x": 337.765625,
                            "y": 197.25
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 110.0,
                            "y": 65.75
                        },
                        "start": {
                            "x": 337.765625,
                            "y": 197.25
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 223.8828125,
                            "y": 0.0
                        },
                        "start": {
                            "x": 223.8828125,
                            "y": 263.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 223.8828125,
                            "y": 0.0
                        },
                        "start": {
                            "x": 223.8828125,
                            "y": 263.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 59.234375,
            "width": 447
        },
        "43": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 110.0,
                            "y": 472.0
                        },
                        "start": {
                            "x": 373.0,
                            "y": 472.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 241.5,
                            "y": 340.5
                        },
                        "start": {
                            "x": 241.5,
                            "y": 603.5
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 60.0,
            "width": 483
        },
        "44": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 110.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 951.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 60.0,
            "width": 220
        },
        "45": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 373.0,
                            "y": 472.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 472.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 60.0,
            "width": 483
        },
        "47": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 110.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 365.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 60.0,
            "width": 475
        },
        "48": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 54.19921875,
                            "y": 579.2001953125
                        },
                        "start": {
                            "x": 54.19921875,
                            "y": 220.759765625
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 54.19921875,
                            "y": 701.330078125
                        },
                        "control2": {
                            "x": 152.87109375,
                            "y": 800.0
                        },
                        "end": {
                            "x": 275.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 54.19921875,
                            "y": 579.2001953125
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 397.130859375,
                            "y": 800.0
                        },
                        "control2": {
                            "x": 495.80078125,
                            "y": 701.330078125
                        },
                        "end": {
                            "x": 495.80078125,
                            "y": 579.2001953125
                        },
                        "start": {
                            "x": 275.0,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 495.80078125,
                            "y": 220.759765625
                        },
                        "start": {
                            "x": 495.80078125,
                            "y": 579.2001953125
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 495.80078125,
                            "y": 98.6298828125
                        },
                        "control2": {
                            "x": 397.130859375,
                            "y": -0.0400390625
                        },
                        "end": {
                            "x": 275.0,
                            "y": -0.0400390625
                        },
                        "start": {
                            "x": 495.80078125,
                            "y": 220.759765625
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 152.87109375,
                            "y": -0.0400390625
                        },
                        "control2": {
                            "x": 54.19921875,
                            "y": 98.6298828125
                        },
                        "end": {
                            "x": 54.19921875,
                            "y": 220.759765625
                        },
                        "start": {
                            "x": 275.0,
                            "y": -0.0400390625
                        }
                    },
                    "type": "C"
                }
            ],
            "leftBearing": 4.19921875,
            "rightBearing": 4.19921875,
            "width": 550
        },
        "49": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 335.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 335.0,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 334.0,
                            "y": 98.0
                        },
                        "control2": {
                            "x": 254.0,
                            "y": 178.0
                        },
                        "end": {
                            "x": 155.0,
                            "y": 178.0
                        },
                        "start": {
                            "x": 335.0,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                }
            ],
            "leftBearing": 105.0,
            "rightBearing": 165.0,
            "width": 550
        },
        "50": {
            "glyph": [
                {
                    "points": {
                        "control1": {
                            "x": 136.0,
                            "y": 78.0
                        },
                        "control2": {
                            "x": 148.0,
                            "y": 61.0
                        },
                        "end": {
                            "x": 163.0,
                            "y": 47.0
                        },
                        "start": {
                            "x": 128.0,
                            "y": 97.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 192.0,
                            "y": 18.0
                        },
                        "control2": {
                            "x": 232.0,
                            "y": 0.0
                        },
                        "end": {
                            "x": 276.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 163.0,
                            "y": 47.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 365.0,
                            "y": 0.0
                        },
                        "control2": {
                            "x": 439.0,
                            "y": 72.0
                        },
                        "end": {
                            "x": 439.0,
                            "y": 162.0
                        },
                        "start": {
                            "x": 276.0,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 439.0,
                            "y": 184.0
                        },
                        "control2": {
                            "x": 431.0,
                            "y": 210.0
                        },
                        "end": {
                            "x": 419.0,
                            "y": 233.0
                        },
                        "start": {
                            "x": 439.0,
                            "y": 162.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 76.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 419.0,
                            "y": 233.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 466.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 76.0,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 26.0,
            "rightBearing": 34.0,
            "width": 550
        },
        "51": {
            "glyph": [
                {
                    "points": {
                        "control1": {
                            "x": 93.0,
                            "y": 80.0
                        },
                        "control2": {
                            "x": 173.0,
                            "y": 0.0
                        },
                        "end": {
                            "x": 272.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 92.0,
                            "y": 178.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 371.0,
                            "y": 0.0
                        },
                        "control2": {
                            "x": 452.0,
                            "y": 81.0
                        },
                        "end": {
                            "x": 452.0,
                            "y": 180.0
                        },
                        "start": {
                            "x": 272.0,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 452.0,
                            "y": 279.0
                        },
                        "control2": {
                            "x": 372.0048828125,
                            "y": 361.0
                        },
                        "end": {
                            "x": 272.0,
                            "y": 361.0
                        },
                        "start": {
                            "x": 452.0,
                            "y": 180.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 393.037109375,
                            "y": 361.0
                        },
                        "control2": {
                            "x": 495.0,
                            "y": 462.0
                        },
                        "end": {
                            "x": 495.0,
                            "y": 582.0
                        },
                        "start": {
                            "x": 272.0,
                            "y": 361.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 495.0,
                            "y": 703.0
                        },
                        "control2": {
                            "x": 396.0,
                            "y": 800.0
                        },
                        "end": {
                            "x": 275.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 495.0,
                            "y": 582.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 153.0,
                            "y": 800.0
                        },
                        "control2": {
                            "x": 55.0,
                            "y": 703.0
                        },
                        "end": {
                            "x": 55.0,
                            "y": 582.0
                        },
                        "start": {
                            "x": 275.0,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                }
            ],
            "leftBearing": 5.0,
            "rightBearing": 5.0,
            "width": 550
        },
        "52": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 16.0,
                            "y": 572.0
                        },
                        "start": {
                            "x": 376.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 525.0,
                            "y": 572.0
                        },
                        "start": {
                            "x": 16.0,
                            "y": 572.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 376.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 376.0,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": -34.0,
            "rightBearing": -25.0,
            "width": 550
        },
        "53": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 117.0,
                            "y": 325.0
                        },
                        "start": {
                            "x": 117.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 117.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 438.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 83.0,
                            "y": 740.0
                        },
                        "control2": {
                            "x": 167.0,
                            "y": 800.0
                        },
                        "end": {
                            "x": 265.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 47.0,
                            "y": 655.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 396.0,
                            "y": 800.0
                        },
                        "control2": {
                            "x": 502.0,
                            "y": 694.0
                        },
                        "end": {
                            "x": 502.0,
                            "y": 563.0
                        },
                        "start": {
                            "x": 265.0,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 502.0,
                            "y": 433.0
                        },
                        "control2": {
                            "x": 398.0,
                            "y": 328.0
                        },
                        "end": {
                            "x": 268.0,
                            "y": 325.0
                        },
                        "start": {
                            "x": 502.0,
                            "y": 563.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 117.0,
                            "y": 325.0
                        },
                        "start": {
                            "x": 268.0,
                            "y": 325.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": -3.0,
            "rightBearing": -2.0,
            "width": 550
        },
        "54": {
            "glyph": [
                {
                    "points": {
                        "control1": {
                            "x": 449.0,
                            "y": 54.0
                        },
                        "control2": {
                            "x": 369.0,
                            "y": 0.0
                        },
                        "end": {
                            "x": 275.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 488.0,
                            "y": 133.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 144.0,
                            "y": 0.0
                        },
                        "control2": {
                            "x": 37.0,
                            "y": 107.0
                        },
                        "end": {
                            "x": 37.0,
                            "y": 238.0
                        },
                        "start": {
                            "x": 275.0,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 37.0,
                            "y": 563.0
                        },
                        "start": {
                            "x": 37.0,
                            "y": 238.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 37.0,
                            "y": 694.0
                        },
                        "control2": {
                            "x": 144.0,
                            "y": 800.0
                        },
                        "end": {
                            "x": 275.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 37.0,
                            "y": 563.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 406.0,
                            "y": 800.0
                        },
                        "control2": {
                            "x": 513.0,
                            "y": 694.0
                        },
                        "end": {
                            "x": 513.0,
                            "y": 563.0
                        },
                        "start": {
                            "x": 275.0,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 513.0,
                            "y": 432.0
                        },
                        "control2": {
                            "x": 406.0,
                            "y": 325.0
                        },
                        "end": {
                            "x": 275.0,
                            "y": 325.0
                        },
                        "start": {
                            "x": 513.0,
                            "y": 563.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 144.0,
                            "y": 325.0
                        },
                        "control2": {
                            "x": 37.0,
                            "y": 432.0
                        },
                        "end": {
                            "x": 37.0,
                            "y": 563.0
                        },
                        "start": {
                            "x": 275.0,
                            "y": 325.0
                        }
                    },
                    "type": "C"
                }
            ],
            "leftBearing": -13.0,
            "rightBearing": -13.0,
            "width": 550
        },
        "55": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 195.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 515.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 515.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 35.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": -15.0,
            "rightBearing": -15.0,
            "width": 550
        },
        "56": {
            "glyph": [
                {
                    "points": {
                        "control1": {
                            "x": 513.0,
                            "y": 693.0
                        },
                        "control2": {
                            "x": 406.0,
                            "y": 800.0
                        },
                        "end": {
                            "x": 275.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 513.0,
                            "y": 562.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 144.0,
                            "y": 800.0
                        },
                        "control2": {
                            "x": 37.0,
                            "y": 693.0
                        },
                        "end": {
                            "x": 37.0,
                            "y": 562.0
                        },
                        "start": {
                            "x": 275.0,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 37.0,
                            "y": 431.0
                        },
                        "control2": {
                            "x": 144.0,
                            "y": 324.0
                        },
                        "end": {
                            "x": 275.0,
                            "y": 324.0
                        },
                        "start": {
                            "x": 37.0,
                            "y": 562.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 406.0,
                            "y": 324.0
                        },
                        "control2": {
                            "x": 513.0,
                            "y": 431.0
                        },
                        "end": {
                            "x": 513.0,
                            "y": 562.0
                        },
                        "start": {
                            "x": 275.0,
                            "y": 324.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 437.0,
                            "y": 251.0
                        },
                        "control2": {
                            "x": 364.0,
                            "y": 324.0
                        },
                        "end": {
                            "x": 275.0,
                            "y": 324.0
                        },
                        "start": {
                            "x": 437.0,
                            "y": 162.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 186.0,
                            "y": 324.0
                        },
                        "control2": {
                            "x": 113.0,
                            "y": 251.0
                        },
                        "end": {
                            "x": 113.0,
                            "y": 162.0
                        },
                        "start": {
                            "x": 275.0,
                            "y": 324.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 113.0,
                            "y": 73.0
                        },
                        "control2": {
                            "x": 186.0,
                            "y": 0.0
                        },
                        "end": {
                            "x": 275.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 113.0,
                            "y": 162.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 364.0,
                            "y": 0.0
                        },
                        "control2": {
                            "x": 437.0,
                            "y": 73.0
                        },
                        "end": {
                            "x": 437.0,
                            "y": 162.0
                        },
                        "start": {
                            "x": 275.0,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                }
            ],
            "leftBearing": -13.0,
            "rightBearing": -13.0,
            "width": 550
        },
        "57": {
            "glyph": [
                {
                    "points": {
                        "control1": {
                            "x": 100.0,
                            "y": 746.0
                        },
                        "control2": {
                            "x": 181.0,
                            "y": 800.0
                        },
                        "end": {
                            "x": 275.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 61.0,
                            "y": 667.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 406.0,
                            "y": 800.0
                        },
                        "control2": {
                            "x": 513.0,
                            "y": 694.0
                        },
                        "end": {
                            "x": 513.0,
                            "y": 563.0
                        },
                        "start": {
                            "x": 275.0,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 513.0,
                            "y": 369.0
                        },
                        "control2": {
                            "x": 406.0,
                            "y": 476.0
                        },
                        "end": {
                            "x": 275.0,
                            "y": 476.0
                        },
                        "start": {
                            "x": 513.0,
                            "y": 238.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 144.0,
                            "y": 476.0
                        },
                        "control2": {
                            "x": 37.0,
                            "y": 369.0
                        },
                        "end": {
                            "x": 37.0,
                            "y": 238.0
                        },
                        "start": {
                            "x": 275.0,
                            "y": 476.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 37.0,
                            "y": 107.0
                        },
                        "control2": {
                            "x": 144.0,
                            "y": 0.0
                        },
                        "end": {
                            "x": 275.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 37.0,
                            "y": 238.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 406.0,
                            "y": 0.0
                        },
                        "control2": {
                            "x": 513.0,
                            "y": 107.0
                        },
                        "end": {
                            "x": 513.0,
                            "y": 238.0
                        },
                        "start": {
                            "x": 275.0,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 513.0,
                            "y": 563.0
                        },
                        "start": {
                            "x": 513.0,
                            "y": 238.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": -13.0,
            "rightBearing": -13.0,
            "width": 550
        },
        "59": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 107.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 107.0,
                            "y": 951.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 57.0,
            "rightBearing": 63.0,
            "width": 220
        },
        "60": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 110.0,
                            "y": 584.0
                        },
                        "start": {
                            "x": 366.0,
                            "y": 802.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 366.0,
                            "y": 366.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 584.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 60.0,
            "width": 476
        },
        "61": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 110.0,
                            "y": 578.0
                        },
                        "start": {
                            "x": 373.0,
                            "y": 578.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 110.0,
                            "y": 366.0
                        },
                        "start": {
                            "x": 373.0,
                            "y": 366.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 60.0,
            "width": 483
        },
        "62": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 366.0,
                            "y": 584.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 802.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 110.0,
                            "y": 366.0
                        },
                        "start": {
                            "x": 366.0,
                            "y": 584.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 60.0,
            "width": 476
        },
        "63": {
            "glyph": [
                {
                    "points": {
                        "control1": {
                            "x": 138.0,
                            "y": 79.0
                        },
                        "control2": {
                            "x": 150.0,
                            "y": 61.0
                        },
                        "end": {
                            "x": 165.0,
                            "y": 47.0
                        },
                        "start": {
                            "x": 130.0,
                            "y": 98.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 194.0,
                            "y": 18.0
                        },
                        "control2": {
                            "x": 235.0,
                            "y": 0.0
                        },
                        "end": {
                            "x": 279.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 165.0,
                            "y": 47.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 369.0,
                            "y": 0.0
                        },
                        "control2": {
                            "x": 441.0,
                            "y": 73.0
                        },
                        "end": {
                            "x": 441.0,
                            "y": 162.0
                        },
                        "start": {
                            "x": 279.0,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 441.0,
                            "y": 184.0
                        },
                        "control2": {
                            "x": 433.0,
                            "y": 210.0
                        },
                        "end": {
                            "x": 421.0,
                            "y": 233.0
                        },
                        "start": {
                            "x": 441.0,
                            "y": 162.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 296.0,
                            "y": 442.0
                        },
                        "start": {
                            "x": 421.0,
                            "y": 233.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 256.6513671875,
                            "y": 507.791015625
                        },
                        "control2": {
                            "x": 249.0,
                            "y": 556.0
                        },
                        "end": {
                            "x": 249.0,
                            "y": 611.0
                        },
                        "start": {
                            "x": 296.0,
                            "y": 442.0
                        }
                    },
                    "type": "C"
                }
            ],
            "leftBearing": 80.0,
            "rightBearing": 40.0,
            "width": 531
        },
        "64": {
            "glyph": [
                {
                    "points": {
                        "control1": {
                            "x": 661.6201171875,
                            "y": 442.8564453125
                        },
                        "control2": {
                            "x": 660.6513671875,
                            "y": 452.37109375
                        },
                        "end": {
                            "x": 660.6513671875,
                            "y": 461.6591796875
                        },
                        "start": {
                            "x": 663.6435546875,
                            "y": 433.2802734375
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 660.6513671875,
                            "y": 512.966796875
                        },
                        "control2": {
                            "x": 705.48046875,
                            "y": 569.4931640625
                        },
                        "end": {
                            "x": 753.744140625,
                            "y": 579.9501953125
                        },
                        "start": {
                            "x": 660.6513671875,
                            "y": 461.6591796875
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 760.0341796875,
                            "y": 581.3125
                        },
                        "control2": {
                            "x": 766.3232421875,
                            "y": 581.9697265625
                        },
                        "end": {
                            "x": 772.556640625,
                            "y": 581.9697265625
                        },
                        "start": {
                            "x": 753.744140625,
                            "y": 579.9501953125
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 822.802734375,
                            "y": 581.9697265625
                        },
                        "control2": {
                            "x": 888.4990234375,
                            "y": 546.1650390625
                        },
                        "end": {
                            "x": 901.84375,
                            "y": 483.0
                        },
                        "start": {
                            "x": 772.556640625,
                            "y": 581.9697265625
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 907.5869140625,
                            "y": 455.43359375
                        },
                        "control2": {
                            "x": 910.349609375,
                            "y": 427.931640625
                        },
                        "end": {
                            "x": 910.349609375,
                            "y": 400.83203125
                        },
                        "start": {
                            "x": 901.84375,
                            "y": 483.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 910.349609375,
                            "y": 215.591796875
                        },
                        "control2": {
                            "x": 781.27734375,
                            "y": 49.12890625
                        },
                        "end": {
                            "x": 592.84375,
                            "y": 9.0
                        },
                        "start": {
                            "x": 910.349609375,
                            "y": 400.83203125
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 564.794921875,
                            "y": 3.0263671875
                        },
                        "control2": {
                            "x": 536.8134765625,
                            "y": 0.15625
                        },
                        "end": {
                            "x": 509.25390625,
                            "y": 0.15625
                        },
                        "start": {
                            "x": 592.84375,
                            "y": 9.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 324.583984375,
                            "y": 0.15625
                        },
                        "control2": {
                            "x": 158.87109375,
                            "y": 129.048828125
                        },
                        "end": {
                            "x": 118.84375,
                            "y": 317.0
                        },
                        "start": {
                            "x": 509.25390625,
                            "y": 0.15625
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 112.87109375,
                            "y": 345.048828125
                        },
                        "control2": {
                            "x": 110.0,
                            "y": 373.03125
                        },
                        "end": {
                            "x": 110.0,
                            "y": 400.5908203125
                        },
                        "start": {
                            "x": 118.84375,
                            "y": 317.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 110.0,
                            "y": 585.259765625
                        },
                        "control2": {
                            "x": 238.8935546875,
                            "y": 750.9736328125
                        },
                        "end": {
                            "x": 426.84375,
                            "y": 791.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 400.5908203125
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 454.7373046875,
                            "y": 796.83203125
                        },
                        "control2": {
                            "x": 482.56640625,
                            "y": 799.642578125
                        },
                        "end": {
                            "x": 509.98828125,
                            "y": 799.642578125
                        },
                        "start": {
                            "x": 426.84375,
                            "y": 791.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 573.0732421875,
                            "y": 799.642578125
                        },
                        "control2": {
                            "x": 634.0068359375,
                            "y": 784.76953125
                        },
                        "end": {
                            "x": 688.6181640625,
                            "y": 757.60546875
                        },
                        "start": {
                            "x": 509.98828125,
                            "y": 799.642578125
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 399.9765625,
                            "y": 551.1083984375
                        },
                        "control2": {
                            "x": 349.9326171875,
                            "y": 482.8564453125
                        },
                        "end": {
                            "x": 349.9326171875,
                            "y": 405.1337890625
                        },
                        "start": {
                            "x": 473.5634765625,
                            "y": 567.0400390625
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 349.9326171875,
                            "y": 392.7001953125
                        },
                        "control2": {
                            "x": 351.2138671875,
                            "y": 380.0234375
                        },
                        "end": {
                            "x": 353.8837890625,
                            "y": 367.2802734375
                        },
                        "start": {
                            "x": 349.9326171875,
                            "y": 405.1337890625
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 370.849609375,
                            "y": 286.306640625
                        },
                        "control2": {
                            "x": 438.5009765625,
                            "y": 231.013671875
                        },
                        "end": {
                            "x": 512.4208984375,
                            "y": 231.013671875
                        },
                        "start": {
                            "x": 353.8837890625,
                            "y": 367.2802734375
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 522.8525390625,
                            "y": 231.013671875
                        },
                        "control2": {
                            "x": 533.408203125,
                            "y": 232.1142578125
                        },
                        "end": {
                            "x": 543.9638671875,
                            "y": 234.400390625
                        },
                        "start": {
                            "x": 512.4208984375,
                            "y": 231.013671875
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 617.5517578125,
                            "y": 250.3310546875
                        },
                        "control2": {
                            "x": 667.5947265625,
                            "y": 317.9287109375
                        },
                        "end": {
                            "x": 667.5947265625,
                            "y": 395.4716796875
                        },
                        "start": {
                            "x": 543.9638671875,
                            "y": 234.400390625
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 667.5947265625,
                            "y": 407.876953125
                        },
                        "control2": {
                            "x": 666.3134765625,
                            "y": 420.5361328125
                        },
                        "end": {
                            "x": 663.6435546875,
                            "y": 433.2802734375
                        },
                        "start": {
                            "x": 667.5947265625,
                            "y": 395.4716796875
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 646.65234375,
                            "y": 514.375
                        },
                        "control2": {
                            "x": 578.8232421875,
                            "y": 570.3896484375
                        },
                        "end": {
                            "x": 504.7744140625,
                            "y": 570.3896484375
                        },
                        "start": {
                            "x": 663.6435546875,
                            "y": 433.2802734375
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 494.451171875,
                            "y": 570.3896484375
                        },
                        "control2": {
                            "x": 484.0078125,
                            "y": 569.30078125
                        },
                        "end": {
                            "x": 473.5634765625,
                            "y": 567.0400390625
                        },
                        "start": {
                            "x": 504.7744140625,
                            "y": 570.3896484375
                        }
                    },
                    "type": "C"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 59.650390625,
            "width": 1020
        },
        "65": {
            "glyph": [
                {
                    "points": {
                        "control1": {
                            "x": 478.0,
                            "y": 488.0
                        },
                        "control2": {
                            "x": 308.0,
                            "y": 0.0
                        },
                        "end": {
                            "x": 308.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 587.0,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 30.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 308.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 492.0,
                            "y": 528.0
                        },
                        "start": {
                            "x": 125.0,
                            "y": 528.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": -20.0,
            "rightBearing": -20.0,
            "width": 617
        },
        "66": {
            "glyph": [
                {
                    "points": {
                        "control1": {
                            "x": 203.9931640625,
                            "y": 320.0
                        },
                        "control2": {
                            "x": 351.9931640625,
                            "y": 320.0
                        },
                        "end": {
                            "x": 351.9931640625,
                            "y": 320.0
                        },
                        "start": {
                            "x": 109.9931640625,
                            "y": 320.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 484.9931640625,
                            "y": 321.0
                        },
                        "control2": {
                            "x": 591.0009765625,
                            "y": 429.0
                        },
                        "end": {
                            "x": 589.9931640625,
                            "y": 562.0
                        },
                        "start": {
                            "x": 351.9931640625,
                            "y": 320.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 588.9931640625,
                            "y": 694.0
                        },
                        "control2": {
                            "x": 481.9931640625,
                            "y": 800.0
                        },
                        "end": {
                            "x": 349.9931640625,
                            "y": 800.0
                        },
                        "start": {
                            "x": 589.9931640625,
                            "y": 562.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 109.9931640625,
                            "y": 799.0
                        },
                        "start": {
                            "x": 349.9931640625,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 109.9931640625,
                            "y": 0.0
                        },
                        "start": {
                            "x": 109.9931640625,
                            "y": 799.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 270.9931640625,
                            "y": 0.0
                        },
                        "start": {
                            "x": 109.9931640625,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 358.9931640625,
                            "y": 1.0
                        },
                        "control2": {
                            "x": 431.0048828125,
                            "y": 73.0
                        },
                        "end": {
                            "x": 429.9931640625,
                            "y": 162.0
                        },
                        "start": {
                            "x": 270.9931640625,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 428.9931640625,
                            "y": 250.0
                        },
                        "control2": {
                            "x": 357.9931640625,
                            "y": 321.0
                        },
                        "end": {
                            "x": 269.9931640625,
                            "y": 321.0
                        },
                        "start": {
                            "x": 429.9931640625,
                            "y": 162.0
                        }
                    },
                    "type": "C"
                }
            ],
            "leftBearing": 59.993164062000005,
            "rightBearing": -0.0002906938993874064,
            "width": 640
        },
        "67": {
            "glyph": [
                {
                    "points": {
                        "control1": {
                            "x": 579.0,
                            "y": 764.0
                        },
                        "control2": {
                            "x": 500.0,
                            "y": 800.0
                        },
                        "end": {
                            "x": 411.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 637.0,
                            "y": 706.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 234.0,
                            "y": 800.0
                        },
                        "control2": {
                            "x": 90.0,
                            "y": 657.0
                        },
                        "end": {
                            "x": 90.0,
                            "y": 480.0
                        },
                        "start": {
                            "x": 411.0,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 90.0,
                            "y": 320.0
                        },
                        "start": {
                            "x": 90.0,
                            "y": 480.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 90.0,
                            "y": 143.0
                        },
                        "control2": {
                            "x": 234.0,
                            "y": 0.0
                        },
                        "end": {
                            "x": 411.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 90.0,
                            "y": 320.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 500.0,
                            "y": 0.0
                        },
                        "control2": {
                            "x": 579.0,
                            "y": 36.0
                        },
                        "end": {
                            "x": 637.0,
                            "y": 94.0
                        },
                        "start": {
                            "x": 411.0,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                }
            ],
            "leftBearing": 40.0,
            "rightBearing": 20.0,
            "width": 707
        },
        "68": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 110.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 310.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 412.0,
                            "y": 0.0
                        },
                        "control2": {
                            "x": 514.0,
                            "y": 39.0
                        },
                        "end": {
                            "x": 592.0,
                            "y": 117.0
                        },
                        "start": {
                            "x": 310.0,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 748.0,
                            "y": 273.0
                        },
                        "control2": {
                            "x": 748.0,
                            "y": 527.0
                        },
                        "end": {
                            "x": 592.0,
                            "y": 683.0
                        },
                        "start": {
                            "x": 592.0,
                            "y": 117.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 514.0,
                            "y": 761.0
                        },
                        "control2": {
                            "x": 410.0,
                            "y": 800.0
                        },
                        "end": {
                            "x": 309.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 592.0,
                            "y": 683.0
                        }
                    },
                    "type": "C"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 20.0,
            "width": 779
        },
        "69": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 470.0,
                            "y": 400.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 400.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 470.0,
                            "y": 400.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 400.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 110.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 550.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 110.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 550.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 10.0,
            "width": 610
        },
        "70": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 470.0,
                            "y": 400.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 400.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 110.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 550.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 110.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": -40.0,
            "width": 560
        },
        "71": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 753.0,
                            "y": 425.0
                        },
                        "start": {
                            "x": 532.0,
                            "y": 425.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 753.0,
                            "y": 683.0
                        },
                        "start": {
                            "x": 753.0,
                            "y": 425.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 675.0,
                            "y": 761.0
                        },
                        "control2": {
                            "x": 572.0,
                            "y": 800.0
                        },
                        "end": {
                            "x": 470.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 753.0,
                            "y": 683.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 368.0,
                            "y": 800.0
                        },
                        "control2": {
                            "x": 265.0,
                            "y": 761.0
                        },
                        "end": {
                            "x": 187.0,
                            "y": 683.0
                        },
                        "start": {
                            "x": 470.0,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 109.0,
                            "y": 605.0
                        },
                        "control2": {
                            "x": 70.0,
                            "y": 502.0
                        },
                        "end": {
                            "x": 70.0,
                            "y": 400.0
                        },
                        "start": {
                            "x": 187.0,
                            "y": 683.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 70.0,
                            "y": 298.0
                        },
                        "control2": {
                            "x": 109.0,
                            "y": 195.0
                        },
                        "end": {
                            "x": 187.0,
                            "y": 117.0
                        },
                        "start": {
                            "x": 70.0,
                            "y": 400.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 265.0,
                            "y": 39.0
                        },
                        "control2": {
                            "x": 367.0,
                            "y": 0.0
                        },
                        "end": {
                            "x": 469.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 187.0,
                            "y": 117.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 571.0,
                            "y": 0.0
                        },
                        "control2": {
                            "x": 674.0,
                            "y": 38.0
                        },
                        "end": {
                            "x": 752.0,
                            "y": 116.0
                        },
                        "start": {
                            "x": 469.0,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                }
            ],
            "leftBearing": 20.0,
            "rightBearing": 40.0,
            "width": 843
        },
        "72": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 588.0,
                            "y": 400.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 400.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 590.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 590.0,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 110.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 60.0,
            "width": 700
        },
        "73": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 122.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 122.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 72.0,
            "rightBearing": 72.0,
            "width": 244
        },
        "74": {
            "glyph": [
                {
                    "points": {
                        "control1": {
                            "x": 38.0,
                            "y": 722.0
                        },
                        "control2": {
                            "x": 50.0,
                            "y": 739.0
                        },
                        "end": {
                            "x": 65.0,
                            "y": 753.0
                        },
                        "start": {
                            "x": 30.0,
                            "y": 703.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 94.0,
                            "y": 782.0
                        },
                        "control2": {
                            "x": 135.0,
                            "y": 800.0
                        },
                        "end": {
                            "x": 179.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 65.0,
                            "y": 753.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 269.0,
                            "y": 800.0
                        },
                        "control2": {
                            "x": 341.0,
                            "y": 728.0
                        },
                        "end": {
                            "x": 341.0,
                            "y": 638.0
                        },
                        "start": {
                            "x": 179.0,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 341.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 341.0,
                            "y": 638.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": -20.0,
            "rightBearing": 40.0,
            "width": 431
        },
        "75": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 110.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 166.0,
                            "y": 400.0
                        },
                        "start": {
                            "x": 570.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 570.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 166.0,
                            "y": 400.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 0.0,
            "width": 620
        },
        "76": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 110.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 590.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": -20.0,
            "width": 620
        },
        "77": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 512.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 912.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 512.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 110.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 912.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 912.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 60.0,
            "width": 1022
        },
        "78": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 670.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 110.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 670.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 670.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 60.0,
            "width": 780
        },
        "79": {
            "glyph": [
                {
                    "points": {
                        "control1": {
                            "x": 249.0,
                            "y": 800.0
                        },
                        "control2": {
                            "x": 70.0,
                            "y": 621.0
                        },
                        "end": {
                            "x": 70.0,
                            "y": 400.0
                        },
                        "start": {
                            "x": 470.0,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 70.0,
                            "y": 179.0
                        },
                        "control2": {
                            "x": 249.0,
                            "y": 0.0
                        },
                        "end": {
                            "x": 470.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 70.0,
                            "y": 400.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 691.0,
                            "y": 0.0
                        },
                        "control2": {
                            "x": 870.0,
                            "y": 179.0
                        },
                        "end": {
                            "x": 870.0,
                            "y": 400.0
                        },
                        "start": {
                            "x": 470.0,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 870.0,
                            "y": 621.0
                        },
                        "control2": {
                            "x": 691.0,
                            "y": 800.0
                        },
                        "end": {
                            "x": 470.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 870.0,
                            "y": 400.0
                        }
                    },
                    "type": "C"
                }
            ],
            "leftBearing": 20.0,
            "rightBearing": 20.0,
            "width": 940
        },
        "80": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 348.0,
                            "y": 480.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 480.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 480.0,
                            "y": 480.0
                        },
                        "control2": {
                            "x": 587.0078125,
                            "y": 374.0
                        },
                        "end": {
                            "x": 588.0,
                            "y": 242.0
                        },
                        "start": {
                            "x": 348.0,
                            "y": 480.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 589.0,
                            "y": 109.0
                        },
                        "control2": {
                            "x": 483.0,
                            "y": 1.0
                        },
                        "end": {
                            "x": 350.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 588.0,
                            "y": 242.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 110.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 350.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 110.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": -0.007017530237817482,
            "width": 638
        },
        "81": {
            "glyph": [
                {
                    "points": {
                        "control1": {
                            "x": 693.0,
                            "y": 800.0
                        },
                        "control2": {
                            "x": 874.0,
                            "y": 621.0
                        },
                        "end": {
                            "x": 874.0,
                            "y": 400.0
                        },
                        "start": {
                            "x": 472.0,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 874.0,
                            "y": 179.0
                        },
                        "control2": {
                            "x": 693.0,
                            "y": 0.0
                        },
                        "end": {
                            "x": 472.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 874.0,
                            "y": 400.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 251.0,
                            "y": 0.0
                        },
                        "control2": {
                            "x": 70.0,
                            "y": 179.0
                        },
                        "end": {
                            "x": 70.0,
                            "y": 400.0
                        },
                        "start": {
                            "x": 472.0,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 70.0,
                            "y": 621.0
                        },
                        "control2": {
                            "x": 251.0,
                            "y": 800.0
                        },
                        "end": {
                            "x": 472.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 70.0,
                            "y": 400.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 872.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 638.0,
                            "y": 566.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 20.0,
            "rightBearing": 20.0,
            "width": 944
        },
        "82": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 288.9912109375,
                            "y": 400.0
                        },
                        "start": {
                            "x": 489.9912109375,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 109.9912109375,
                            "y": 0.0
                        },
                        "start": {
                            "x": 109.9912109375,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 311.9912109375,
                            "y": 0.0
                        },
                        "start": {
                            "x": 109.9912109375,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 422.9912109375,
                            "y": 0.0
                        },
                        "control2": {
                            "x": 511.0,
                            "y": 91.0
                        },
                        "end": {
                            "x": 509.9912109375,
                            "y": 202.0
                        },
                        "start": {
                            "x": 311.9912109375,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 508.9912109375,
                            "y": 312.0
                        },
                        "control2": {
                            "x": 419.9912109375,
                            "y": 400.0
                        },
                        "end": {
                            "x": 309.9912109375,
                            "y": 400.0
                        },
                        "start": {
                            "x": 509.9912109375,
                            "y": 202.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 309.9912109375,
                            "y": 400.0
                        },
                        "control2": {
                            "x": 187.9912109375,
                            "y": 400.0
                        },
                        "end": {
                            "x": 109.9912109375,
                            "y": 400.0
                        },
                        "start": {
                            "x": 309.9912109375,
                            "y": 400.0
                        }
                    },
                    "type": "C"
                }
            ],
            "leftBearing": 59.991210937999995,
            "rightBearing": 20.000202958291766,
            "width": 580
        },
        "83": {
            "glyph": [
                {
                    "points": {
                        "control1": {
                            "x": 80.0,
                            "y": 703.0
                        },
                        "control2": {
                            "x": 94.0,
                            "y": 724.0
                        },
                        "end": {
                            "x": 112.0,
                            "y": 742.0
                        },
                        "start": {
                            "x": 70.0,
                            "y": 679.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 148.0,
                            "y": 778.0
                        },
                        "control2": {
                            "x": 198.0,
                            "y": 800.0
                        },
                        "end": {
                            "x": 253.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 112.0,
                            "y": 742.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 362.0,
                            "y": 800.0
                        },
                        "control2": {
                            "x": 450.0,
                            "y": 711.0
                        },
                        "end": {
                            "x": 450.0,
                            "y": 602.0
                        },
                        "start": {
                            "x": 253.0,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 450.0,
                            "y": 548.0
                        },
                        "control2": {
                            "x": 429.7685546875,
                            "y": 490.943359375
                        },
                        "end": {
                            "x": 379.0,
                            "y": 454.0
                        },
                        "start": {
                            "x": 450.0,
                            "y": 602.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 155.0,
                            "y": 291.0
                        },
                        "start": {
                            "x": 379.0,
                            "y": 454.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 118.0,
                            "y": 264.0
                        },
                        "control2": {
                            "x": 88.0,
                            "y": 207.0
                        },
                        "end": {
                            "x": 88.0,
                            "y": 162.0
                        },
                        "start": {
                            "x": 155.0,
                            "y": 291.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 88.0,
                            "y": 73.0
                        },
                        "control2": {
                            "x": 160.0,
                            "y": 0.0
                        },
                        "end": {
                            "x": 250.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 88.0,
                            "y": 162.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 294.0,
                            "y": 0.0
                        },
                        "control2": {
                            "x": 335.0,
                            "y": 18.0
                        },
                        "end": {
                            "x": 364.0,
                            "y": 47.0
                        },
                        "start": {
                            "x": 250.0,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 379.0,
                            "y": 61.0
                        },
                        "control2": {
                            "x": 391.0,
                            "y": 78.0
                        },
                        "end": {
                            "x": 399.0,
                            "y": 97.0
                        },
                        "start": {
                            "x": 364.0,
                            "y": 47.0
                        }
                    },
                    "type": "C"
                }
            ],
            "leftBearing": 20.0,
            "rightBearing": 20.0,
            "width": 520
        },
        "84": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 310.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 310.0,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 630.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": -10.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": -60.0,
            "rightBearing": -60.0,
            "width": 620
        },
        "85": {
            "glyph": [
                {
                    "points": {
                        "control1": {
                            "x": 730.0,
                            "y": 185.0
                        },
                        "control2": {
                            "x": 730.0,
                            "y": 480.0
                        },
                        "end": {
                            "x": 730.0,
                            "y": 480.0
                        },
                        "start": {
                            "x": 730.0,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 730.0,
                            "y": 657.0
                        },
                        "control2": {
                            "x": 587.0,
                            "y": 800.0
                        },
                        "end": {
                            "x": 410.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 730.0,
                            "y": 480.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 233.0,
                            "y": 800.0
                        },
                        "control2": {
                            "x": 90.0,
                            "y": 656.8974609375
                        },
                        "end": {
                            "x": 90.0,
                            "y": 480.0
                        },
                        "start": {
                            "x": 410.0,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 90.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 90.0,
                            "y": 480.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 40.0,
            "rightBearing": 40.0,
            "width": 820
        },
        "86": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 330.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 30.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 630.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 330.0,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": -20.0,
            "rightBearing": -20.0,
            "width": 660
        },
        "87": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 517.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 273.0,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 273.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 30.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 1004.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 760.0,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 760.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 517.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": -20.0,
            "rightBearing": -20.0,
            "width": 1034
        },
        "88": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 618.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 90.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 618.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 90.0,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 40.0,
            "rightBearing": 40.0,
            "width": 708
        },
        "89": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 319.0,
                            "y": 400.0
                        },
                        "start": {
                            "x": 319.0,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 319.0,
                            "y": 400.0
                        },
                        "start": {
                            "x": 30.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 609.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 319.0,
                            "y": 400.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": -20.0,
            "rightBearing": -20.0,
            "width": 639
        },
        "90": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 90.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 570.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 570.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 90.0,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 570.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 90.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 40.0,
            "rightBearing": 40.0,
            "width": 660
        },
        "91": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 110.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 182.0,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 111.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 183.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 111.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 60.0,
            "width": 293
        },
        "92": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 366.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 60.0,
            "width": 476
        },
        "93": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 183.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 111.0,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 182.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 183.0,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 110.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 182.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 60.0,
            "width": 293
        },
        "94": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 263.0,
                            "y": -171.099609375
                        },
                        "start": {
                            "x": 110.0,
                            "y": -64.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 416.0,
                            "y": -64.0
                        },
                        "start": {
                            "x": 263.0,
                            "y": -171.099609375
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 60.0,
            "width": 526
        },
        "95": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 525.0,
                            "y": 1000.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": 1000.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 60.0,
            "width": 635
        },
        "96": {
            "glyph": [
                {
                    "points": {
                        "end": {
                            "x": 263.0,
                            "y": -64.0
                        },
                        "start": {
                            "x": 110.0,
                            "y": -171.099609375
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": 60.0,
            "rightBearing": 60.0,
            "width": 373
        },
        "97": {
            "glyph": [
                {
                    "points": {
                        "control1": {
                            "x": 478.0,
                            "y": 488.0
                        },
                        "control2": {
                            "x": 308.0,
                            "y": 0.0
                        },
                        "end": {
                            "x": 308.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 587.0,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 30.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 308.0,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 492.0,
                            "y": 528.0
                        },
                        "start": {
                            "x": 125.0,
                            "y": 528.0
                        }
                    },
                    "type": "L"
                }
            ],
            "leftBearing": -20.0,
            "rightBearing": -20.0,
            "width": 617
        },
        "98": {
            "glyph": [
                {
                    "points": {
                        "control1": {
                            "x": 203.9931640625,
                            "y": 320.0
                        },
                        "control2": {
                            "x": 351.9931640625,
                            "y": 320.0
                        },
                        "end": {
                            "x": 351.9931640625,
                            "y": 320.0
                        },
                        "start": {
                            "x": 109.9931640625,
                            "y": 320.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 484.9931640625,
                            "y": 321.0
                        },
                        "control2": {
                            "x": 591.0009765625,
                            "y": 429.0
                        },
                        "end": {
                            "x": 589.9931640625,
                            "y": 562.0
                        },
                        "start": {
                            "x": 351.9931640625,
                            "y": 320.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 588.9931640625,
                            "y": 694.0
                        },
                        "control2": {
                            "x": 481.9931640625,
                            "y": 800.0
                        },
                        "end": {
                            "x": 349.9931640625,
                            "y": 800.0
                        },
                        "start": {
                            "x": 589.9931640625,
                            "y": 562.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 109.9931640625,
                            "y": 799.0
                        },
                        "start": {
                            "x": 349.9931640625,
                            "y": 800.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 109.9931640625,
                            "y": 0.0
                        },
                        "start": {
                            "x": 109.9931640625,
                            "y": 799.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "end": {
                            "x": 270.9931640625,
                            "y": 0.0
                        },
                        "start": {
                            "x": 109.9931640625,
                            "y": 0.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 358.9931640625,
                            "y": 1.0
                        },
                        "control2": {
                            "x": 431.0048828125,
                            "y": 73.0
                        },
                        "end": {
                            "x": 429.9931640625,
                            "y": 162.0
                        },
                        "start": {
                            "x": 270.9931640625,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 428.9931640625,
                            "y": 250.0
                        },
                        "control2": {
                            "x": 357.9931640625,
                            "y": 321.0
                        },
                        "end": {
                            "x": 269.9931640625,
                            "y": 321.0
                        },
                        "start": {
                            "x": 429.9931640625,
                            "y": 162.0
                        }
                    },
                    "type": "C"
                }
            ],
            "leftBearing": 59.9931640625,
            "rightBearing": -0.0002906943993821187,
            "width": 640
        },
        "99": {
            "glyph": [
                {
                    "points": {
                        "control1": {
                            "x": 579.0,
                            "y": 764.0
                        },
                        "control2": {
                            "x": 500.0,
                            "y": 800.0
                        },
                        "end": {
                            "x": 411.0,
                            "y": 800.0
                        },
                        "start": {
                            "x": 637.0,
                            "y": 706.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 234.0,
                            "y": 800.0
                        },
                        "control2": {
                            "x": 90.0,
                            "y": 657.0
                        },
                        "end": {
                            "x": 90.0,
                            "y": 480.0
                        },
                        "start": {
                            "x": 411.0,
                            "y": 800.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "end": {
                            "x": 90.0,
                            "y": 320.0
                        },
                        "start": {
                            "x": 90.0,
                            "y": 480.0
                        }
                    },
                    "type": "L"
                },
                {
                    "points": {
                        "control1": {
                            "x": 90.0,
                            "y": 143.0
                        },
                        "control2": {
                            "x": 234.0,
                            "y": 0.0
                        },
                        "end": {
                            "x": 411.0,
                            "y": 0.0
                        },
                        "start": {
                            "x": 90.0,
                            "y": 320.0
                        }
                    },
                    "type": "C"
                },
                {
                    "points": {
                        "control1": {
                            "x": 500.0,
                            "y": 0.0
                        },
                        "control2": {
                            "x": 579.0,
                            "y": 36.0
                        },
                        "end": {
                            "x": 637.0,
                            "y": 94.0
                        },
                        "start": {
                            "x": 411.0,
                            "y": 0.0
                        }
                    },
                    "type": "C"
                }
            ],
            "leftBearing": 40.0,
            "rightBearing": 20.0,
            "width": 707
        }
    }
}
;
