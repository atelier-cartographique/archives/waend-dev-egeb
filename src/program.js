/**
 *
 */

// workerContext.waend = {
//     'Turf': Turf,
//     'Projection': Projection,
//     'Geometry': Geometry,
//     'Transform': Transform,
//     'proj3857': Proj3857,
//     'polygonTransform' : polygonTransform,
//     'lineTransform': lineTransform,
//     'polygonProject': polygonProject,
//     'lineProject': lineProject,
//     'polygonFloor': polygonFloor,
//     'lineFloor': lineFloor,
//     'drawTextOnLine': drawTextOnLine,
//     'drawTextInPolygon': drawTextInPolygon,
//     'drawTextInPolygonAuto': drawTextInPolygonAuto,
//     'emit': emit,
//     'getProperty': getProperty,
//     'processStyle': processStyle
// };


var Text = require('./Text');
var _ = require('./lodash.custom');
var rbush = require('rbush');

var Geometry = waend.Geometry;

var currentExtent,
    currentTransform,
    fm,
    isStarted = false;


function copy (a) {
    return JSON.parse(JSON.stringify(a));
}

var DATA_URL = 'http://egeb-irsnb.geo.ms/sources/7evHZLeZt6DFJjHNo.geojson';

function getJSON (response) {
    return response.json();
}

function isZero (val) {
    return Math.abs(val) <= 1e-12;
}

function lineIntersect(apx, apy, avx, avy, bpx, bpy, bvx, bvy, asVector,
        isInfinite) {
    // Convert 2nd points to vectors if they are not specified as such.
    if (!asVector) {
        avx -= apx;
        avy -= apy;
        bvx -= bpx;
        bvy -= bpy;
    }
    var cross = avx * bvy - avy * bvx;
    // Avoid divisions by 0, and errors when getting too close to 0
    if (!isZero(cross)) {
        var dx = apx - bpx,
            dy = apy - bpy,
            ta = (bvx * dy - bvy * dx) / cross,
            tb = (avx * dy - avy * dx) / cross;
        // Check the ranges of t parameters if the line is not allowed
        // to extend beyond the definition points.
        if (isInfinite || 0 <= ta && ta <= 1 && 0 <= tb && tb <= 1)
            return [apx + ta * avx, apy + ta * avy];
    }
}

function getLineCenter (feature) {
    var fcoords = feature.geometry.coordinates;
    if (fcoords.length === 2) {
        return [
            fcoords[0][0] + ((fcoords[1][0] - fcoords[0][0]) / 2),
            fcoords[0][1] + ((fcoords[1][1] - fcoords[0][1]) / 2)
        ]
    }
    var cidx = Math.floor(fcoords.length / 2);
    return copy(fcoords[cidx]);
}

function coordinatesLefter(a, b) {
  return (a[0] - b[0]);
}

function findIntersectSegment (coordinates, apx, apy, avx, avy) {
    var ret = [], ring, bpx, bpy, bvx, bvy, cv, r;
    for (var i = 0; i < coordinates.length; i++) {
        ring = coordinates[i];
        for (var vi = 1; vi < ring.length; vi++) {
            bpx = ring[vi-1][0];
            bpy = ring[vi-1][1];
            bvx = ring[vi][0] - bpx;
            bvy = ring[vi][1] - bpy;
            r = lineIntersect(apx, apy, avx, avy, bpx, bpy, bvx, bvy, true, false);
            if (r) {
                ret.push(r);
            }
        }
    }
    ret.sort(coordinatesLefter);
    return ret;
}

function getWritableSegments (p, lineHeight, start) {
    var coordinates = p.getCoordinates(),
        extent = p.getExtent(),
        height = extent.getHeight(),
        width = extent.getWidth(),
        bottomLeft = extent.getBottomLeft().getCoordinates(),
        topRight = extent.getTopRight().getCoordinates(),
        left = bottomLeft[0],
        right = topRight[0],
        top = topRight[1],
        segments = [];
    // .log('segments', top, left, width, height);
    start = (start || 0) + 1;
    var offset = start * lineHeight;
    if(offset > height) {
        return null;
    }
    var intersections = findIntersectSegment(coordinates,
        left, top - offset, width, 0);
    for (var i = 1; i < intersections.length; i+=2) {
        segments.push([intersections[i-1], intersections[i]]);
    }

    return segments;
}



function transformCommand (instructions, transforms, cmd) {
    var tfn, p0, p1, p2;

    if (!_.isArray(transforms)) {
        tfn = transforms;
    }
    else if (1 === transforms.length) {
        tfn = transforms[0];
    }
    else {
        tfn = _.flowRight(transforms);
    }
    switch (cmd.type) {
        case 'M':
        p0 = tfn([cmd.x, cmd.y]);
        instructions.push(['moveTo'].concat(p0));
        break;

        case 'L':
        p0 = tfn([cmd.x, cmd.y]);
        instructions.push(['lineTo'].concat(p0));
        break;

        case 'C':
        p0 = tfn([cmd.x1, cmd.y1]);
        p1 = tfn([cmd.x2, cmd.y2]);
        p2 = tfn([cmd.x, cmd.y]);
        instructions.push(['bezierCurveTo',
                            p0[0], p0[1], p1[0], p1[1], p2[0], p2[1]]);
        break;

        case 'Q':
        p0 = tfn([cmd.x1, cmd.y1]);
        p1 = tfn([cmd.x, cmd.y]);
        instructions.push(['quadraticCurveTo',
                            p0[0], p0[1], p1[0], p1[1]]);
        break;

        case 'Z':
        instructions.push(['closePath']);
        return null;
    }

    var minx = Number.MAX_VALUE, maxx = Number.MIN_VALUE,
        miny = Number.MAX_VALUE, maxy = Number.MIN_VALUE;
    var points = [p0, p1, p2];
    for (var i = 0; i < points.length; i++) {
        var p = points[i];
        if (p) {
            minx = Math.min(minx, p[0]);
            maxx = Math.max(maxx, p[0]);
            miny = Math.min(miny, p[1]);
            maxy = Math.max(maxy, p[1]);
        }
    }
    return [minx, miny, maxx, maxy];
}


function drawTextInPolygon (T, polygon, txt, fs, dryRun) {
    var startSegment = 0,
        endPos = null,
        lineHeightFactor = 1.5,
        segments = getWritableSegments(polygon, fs * lineHeightFactor , startSegment),
        t = (txt instanceof Text) ? txt : (new Text(txt)),
        result, cursor = t.cursor(),
        paths, p, cmd,
        tfn = T.mapVec2Fn(),
        instructions = [],
        extent = null;

    while (segments) {
        if( !cursor) {
            break;
        }
        if (segments.length > 0) {
            result = t.draw(fs, segments, cursor);
            cursor = result[0];
            paths = result[1];

            for (var i = 0; i < paths.length; i++) {
                p = paths[i];
                var pinsts = [];
                pinsts.push(['beginPath']);
                for (var ii = 0; ii < p.commands.length; ii++) {
                    var pext = transformCommand(pinsts, [tfn], p.commands[ii]);
                    if (pext) {
                        if (!extent) {
                            extent = new Geometry.Extent(pext);
                        }
                        else {
                            extent.add(pext);
                        }
                    }
                }
                pinsts.push(['stroke']);
                instructions.push(pinsts);
            }
        }
        startSegment += 1;
        segments = getWritableSegments(polygon, fs * lineHeightFactor, startSegment);
    }

    var renderInstructions = function (instGroup) {
        if (instGroup.length > 0) {
            var insts = instGroup.shift();
            ctx.emit('instructions', insts);
            renderInstructions(instGroup);
        }
    };
    if (!dryRun) {
        renderInstructions(instructions);
    }
    return extent;
}


function Grid (x, y, hn, vn, hs, vs) {
    Object.defineProperty(this, 'x', {value: x});
    Object.defineProperty(this, 'y', {value: y});
    Object.defineProperty(this, 'width', {value: hn});
    Object.defineProperty(this, 'height', {value: vn});
    Object.defineProperty(this, 'horizontalStep', {value: hs});
    Object.defineProperty(this, 'verticalStep', {value: vs});
}

Grid.prototype.getCoordinates = function (x, y) {
    var xOffset = x * this.horizontalStep;
    var yOffset = y * this.verticalStep;
    return [this.x + xOffset, this.y - yOffset];
};

Grid.prototype.inserText = function (text, fs, x, y, w, h, style_opt) {
    var xOffset = x * this.horizontalStep;
    var yOffset = y * this.verticalStep;
    var width = (w || 1) * this.horizontalStep;
    var height = (h || 1) * this.verticalStep;
    var style = _.defaults({}, style_opt, {strokeStyle: 'black'});
    var background = style.background;
    var ct = currentTransform.clone();
    var scale = ct.getScale()[0];
    var coords = [[
        [this.x + xOffset, this.y - yOffset], // tl
        [this.x + xOffset + width, this.y - yOffset], // tr
        [this.x + xOffset + width, this.y - (yOffset + height)], // br
        [this.x + xOffset, this.y - (yOffset + height)], // bl
        [this.x + xOffset, this.y - yOffset]
    ]];
    // console.log(coords[0]);

    var ply = new ctx.Geometry.Polygon(ctx.polygonProject(coords));

    // var debugCoords = ply.clone().getCoordinates();
    // ctx.polygonTransform(currentTransform, debugCoords);
    // ctx.emit('draw', 'polygon', debugCoords, ['closePath', 'stroke']);

    ctx.emit('save');
    _.forEach(style, function(val, k){
        ctx.emit('set', k, val);
    });
    if (background) {
        var extent = drawTextInPolygon(ct, ply, text, fs / scale, true);
        if (extent) {
            var cartouche = extent.toPolygon();
            ctx.emit('save');
            ctx.emit('set', 'fillStyle', background);
            ctx.emit('draw', 'polygon',
                cartouche.getCoordinates(),
                ['closePath', 'fill']);
            ctx.emit('restore');
        }
    }
    drawTextInPolygon(ct, ply, text, fs / scale);
    ctx.emit('restore');
};

var basePictCursor = [0, 20, 4, 4];

function init (data) {
    console.log('egeb init', data);
    var features = data.features;
    var tree = rbush();
    var gridCursor;
    var grid;
    var pictCursor;
    var gs = [24, 24];
    var currentFrame = null;

    Object.freeze(features);

    for (var i = 0; i < features.length; i++) {
        var feature = features[i];
        var geom = new Geometry.Geometry(feature);
        if ('LineString' === feature.geometry.type) {
            var coords = geom.getCoordinates();
            for (var j = 0; j < coords.length; j++) {
                var c0 = coords[j];
                var lext = new Geometry.Extent([c0[0], c0[1], c0[0], c0[1]]);
                // lext.normalize();
                tree.insert(_.extend({id: i}, lext.getDictionary()));
            }
        }
        else {
            var extent = _.extend({id: i}, geom.getExtent().getDictionary());
            // console.log('egeb insert', extent);
            tree.insert(extent);
        }
    }

    var getFeatures = function (extent) {
        var items = _.uniqBy(tree.search(extent.getDictionary()), function (item) {
            return item.id;
        });
        var result = [];
        items.forEach(function (item) {
            result.push(features[item.id]);
        });
        return result;
    };

    var pictureCursorIncrement = function (c) {
        var x = c[0], y = c[1];
        var w = c[2], h = c[3];
        if ((x + w) > grid.width) {
            x = 0;
            y -= h;
        }
        else {
            x += w;
        }
        return [x, y, w, h];
    }

    var applyStyle = function (style) {
        _.forEach(style, function (val, key) {
            ctx.emit('set', key, val);
        });
    }

    var drawPolygon = function (coordinates, opt_style) {
        var style = _.defaults({}, opt_style, {
            strokeStyle: '#588DC6',
            fillStyle: 'rgba(88,141,198,.8)'
        });
        ctx.polygonProject(coordinates);
        ctx.polygonTransform(currentTransform, coordinates);
        ctx.emit('save');
        applyStyle(style);
        ctx.emit('draw', 'polygon', coordinates, ['closePath', 'stroke', 'fill']);
        ctx.emit('restore');
    }

    var drawLineString = function (coordinates, opt_style) {
        var style = _.defaults({}, opt_style, {
            strokeStyle: '#588DC6',
            lineWidth: 6
        });
        ctx.lineProject(coordinates);
        ctx.lineTransform(currentTransform, coordinates);
        ctx.emit('save');
        applyStyle(style);
        ctx.emit('draw', 'line', coordinates);
        ctx.emit('restore');
    };

    var drawPoint = function (coordinates, radius, opt_style) {
        coordinates = ctx.proj3857.forward(coordinates);
        var x = coordinates[0];
        var y = coordinates[1];
        var coords = [[
            [x - radius, y ],
            [x , y - radius],
            [x + radius, y],
            [x , y + radius]
        ]];
        var style = _.defaults({}, opt_style, {
            fillStyle: '#588DC6'
        });
        ctx.polygonTransform(currentTransform, coords);
        ctx.emit('save');
        applyStyle(style);
        ctx.emit('draw', 'polygon', coords, ['closePath', 'fill']);
        ctx.emit('restore');
    };

    var displayPhoto = function (ref, x, y, width, height) {
        var min = grid.getCoordinates(x, y),
            max = grid.getCoordinates(x + width, y + height);

        var icoords = [[
            [min[0], min[1]],
            [max[0], min[1]],
            [max[0], max[1]],
            [min[0], max[1]],
            [min[0], min[1]]
        ]];
        ctx.polygonProject(icoords);
        ctx.polygonTransform(currentTransform, icoords);

        var pgeom = new Geometry.Polygon(icoords);
        var pextent = pgeom.getExtent().getArray();

        const options = {
            'image': ref,
            'clip' : true,
            'adjust': 'cover',
            'rotation': false
        };
        ctx.emit('image', icoords, pextent, options);
    };

    var insertOutlinedText = function (txt, sz, x, y, w, h, style, outlineColor) {
        outlineColor = outlineColor || 'white';
        var baseStyle = copy(style);
        var outlineStyle = copy(style);
        baseStyle.globalCompositeOperation = 'source-over';
        outlineStyle.globalCompositeOperation = 'source-over';
        outlineStyle.lineCap = 'round';
        outlineStyle.strokeStyle = outlineColor;
        outlineStyle.lineWidth = style.lineWidth + 2;
        grid.inserText(txt, sz, x, y, w, h, outlineStyle);
        grid.inserText(txt, sz, x, y, w, h, baseStyle);
    };

    var setGrid = function (frameId) {

        var gw = currentExtent.getWidth();
        var gh = currentExtent.getHeight();
        var tl = currentExtent.getTopLeft().getCoordinates();
        var br = currentExtent.getBottomRight().getCoordinates();
        var horiStep = gw / gs[0];
        var vertStep = gh / gs[1];

        grid = new Grid(tl[0], tl[1], gs[0], gs[1], horiStep, vertStep);
        gridCursor = [grid.width - 1, 1];
return;
        // var minX = tl[0];
        // var maxX = br[0];
        // var minY = tl[1];
        // var maxY = br[1];
        //
        // var instructions = [];
        // if (frameId !== currentFrame) {
        //     return;
        // }
        // instructions.push(['save']);
        // ctx.emit('set', 'strokeStyle', '#F7FFBD');
        // ctx.emit('set', 'lineWidth', '0.5');
        //
        // var coords;
        //
        // for (var i = 1; i < gs[0]; i++) {
        //     coords = [
        //         [minX + (i * horiStep), minY],
        //         [minX + (i * horiStep), maxY]
        //     ];
        //     ctx.lineProject(coords);
        //     ctx.lineTransform(currentTransform, coords);
        //     instructions.push(['draw', 'line', coords]);
        // }
        // for (var i = 1; i < gs[1]; i++) {
        //     coords = [
        //         [minX, maxY + (i * vertStep)],
        //         [maxX, maxY + (i * vertStep)]
        //     ];
        //     ctx.lineProject(coords);
        //     ctx.lineTransform(currentTransform, coords);
        //     instructions.push(['draw', 'line', coords]);
        // }
        //
        // instructions.push(['restore']);
        // instructions.forEach(function (instr) {
        //     if (frameId !== currentFrame) {
        //         return;
        //     }
        //     ctx.emit.apply(ctx, instr);
        // });
    };

    var isFakePoint = function (feature) {
        if ('Polygon' === feature.geometry.type) {
            var ring = feature.geometry.coordinates[0];
            var x = ring[0][0];
            var y = ring[0][1];
            for (var i = 1; i < ring.length; i++) {
                if ((x !== ring[i][0]) || (y !== ring[i][1])) {
                    return false;
                }
                x = ring[i][0];
                y = ring[i][1];
            }
            return true;
        }
        return false;
    }

    var drawFeatures = function (frameId) {
        var visibleFeatures = getFeatures(currentExtent);
        var bufVal = currentExtent.getWidth() * -0.7;
        var centeredFeatures = getFeatures(currentExtent.clone().buffer(bufVal));
        var currentScale = currentTransform.getScale()[0];

        if (currentScale < 4 && centeredFeatures.length > 1) {
            if (frameId !== currentFrame) {
                return;
            }
            ctx.emit('save');
            ctx.emit('set', 'globalCompositeOperation', 'source-over');

            visibleFeatures.forEach(function (sourceFeature) {
                if (frameId !== currentFrame) {
                    return false;
                }
                var feature = copy(sourceFeature);
                if (isFakePoint(feature)) {
                    return;
                }
                var titre = feature.properties.titre;

                if ('photo' in feature.properties) {
                    displayPhoto(feature.properties.photo,
                        pictCursor[0], pictCursor[1],
                        pictCursor[2], pictCursor[3]
                    );
                    pictCursor = pictureCursorIncrement(pictCursor);
                }

                var cellWidth = 5;
                var cellHeight = 2;
                var x = gridCursor[0] - (cellWidth + 1),
                    y = gridCursor[1];

                var cellCoords = [
                    grid.getCoordinates(x, y),
                    grid.getCoordinates(x + cellWidth + 1, y),
                    grid.getCoordinates(x + cellWidth + 1, y + cellHeight),
                    grid.getCoordinates(x, y + cellHeight),
                    grid.getCoordinates(x, y)
                ];

                var filet = [
                    grid.getCoordinates(x , y),
                    grid.getCoordinates(x , y),
                    grid.getCoordinates(x , y + cellHeight),
                    grid.getCoordinates(x , y + cellHeight),
                ];

                var geom = new Geometry.Geometry(feature);
                var gcoords = grid.getCoordinates(x, y + (cellHeight / 2));
                var fcenter = geom.getExtent().getCenter().getCoordinates();
                if ('LineString' === feature.geometry.type) {
                    fcenter = getLineCenter(feature);
                }
                var line = [gcoords, fcenter];

                ctx.lineProject(line);
                ctx.lineTransform(currentTransform, line);
                ctx.lineProject(filet);
                ctx.lineTransform(currentTransform, filet);
                ctx.lineProject(cellCoords);
                ctx.lineTransform(currentTransform, cellCoords);

                filet[0][0] += 8;
                filet[3][0] += 8;

                ctx.emit('save');
                // ctx.emit('set', 'strokeStyle', 'white');
                // ctx.emit('set', 'fillStyle', 'white');
                // ctx.emit('set', 'lineWidth', '3');
                // ctx.emit('draw', 'line', line);
                // ctx.emit('draw', 'polygon', [cellCoords], ['closePath', 'fill']);

                ctx.emit('set', 'strokeStyle', '#588DC6');
                ctx.emit('set', 'lineWidth', '1');
                ctx.emit('draw', 'line', line);
                ctx.emit('draw', 'line', filet);

                ctx.emit('restore');

                grid.inserText(titre, 16, x + .2 , y - .2,
                    cellWidth, cellHeight, {
                    lineWidth: 1.8
                });

                if ('Polygon' === feature.geometry.type) {
                    drawPolygon(feature.geometry.coordinates, {});
                }
                else if ('LineString' === feature.geometry.type) {
                    drawLineString(feature.geometry.coordinates, {});
                }
                else if ('Point' === feature.geometry.type) {
                    drawPoint(feature.geometry.coordinates, 3);
                }
                gridCursor[1] += cellHeight + 1;
            });
            ctx.emit('restore');
        }
        else {
            var minDist = Number.MAX_VALUE;
            var feature = null;
            var center = currentExtent.getCenter().toGeoJSON();
            _.forEach(centeredFeatures, function (f) {
                var c = ctx.Turf.centroid(f);
                var d = ctx.Turf.distance(center, c);
                if (d < minDist) {
                    minDist = d;
                    feature = copy(f);
                }
            });

            if (feature) {
                var props = feature.properties
                var titre = props.titre;
                var descriptif = props.descriptif;
                var objectifs = props.objectifs;
                var body = objectifs + '\n\n' + descriptif;

                if ('photo' in props) {
                    displayPhoto(props.photo, 0, 0, grid.width / 2, grid.height);
                }

                setTimeout(function () {
                    var titleSZ = 64;

                    insertOutlinedText(titre, titleSZ,
                        1, -0.8,
                        10, 24, {
                            lineWidth: 6
                        });

                    grid.inserText(body, 20,
                        13, 2,
                        8, 16, {
                            lineWidth: 1.8,
                            background: 'white'
                        });

                    if ('Polygon' === feature.geometry.type) {
                        drawPolygon(feature.geometry.coordinates, {
                            fillStyle: 'white'
                        });
                    }
                    else if ('LineString' === feature.geometry.type) {
                        drawLineString(feature.geometry.coordinates, {
                            lineWidth: 2
                        });
                    }
                    else if ('Point' === feature.geometry.type) {
                        // drawPoint(feature.geometry.coordinates, 24);
                    }
                }, 1200);
        }

        }
    };

    ctx.startFrame = function (frameId, opt_extent, opt_matrix, opt_features) {
        currentFrame = frameId;
        fm = opt_matrix;
        pictCursor = copy(basePictCursor);
        currentTransform = new ctx.Transform(opt_matrix);
        currentExtent = new ctx.Geometry.Extent(opt_extent);
        console.log('egeb startFrame', currentTransform.getScale());
        setGrid(frameId);
        setTimeout(function () {
            drawFeatures(frameId);
        }, 1);
    };
}





// fetch(DATA_URL)
//     .then(getJSON)
//     .then(init);



var dev_data = require('./target');
console.log('egeb data', dev_data);
init({"features" : dev_data});
