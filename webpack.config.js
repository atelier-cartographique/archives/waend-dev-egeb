
var path = require('path');
var webpack = require('webpack');
var uglify = new webpack.optimize.UglifyJsPlugin();

var ctx_path = path.resolve(__dirname, "src");
module.exports = {
    context: ctx_path,
    entry: "./program.js",
    output: {
        path: __dirname,
        filename: "bundle.js"
    },
    // plugins: [uglify]
};
